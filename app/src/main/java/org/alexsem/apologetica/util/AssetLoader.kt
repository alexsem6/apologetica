package org.alexsem.apologetica.util

import android.content.Context
import android.graphics.Typeface
import org.alexsem.apologetica.model.Book
import java.io.IOException
import java.util.*

private fun getFontPath(name: String) = String.format("fonts/%s.ttf", name)
private fun getMetadataPath(id: Int) = String.format("books/%03d/book.properties", id)
private fun getTextPath(id: Int) = String.format("books/%03d/text.txt", id)


/**
 * Get number of books
 * @param context Context
 */
fun getBookCount(context: Context) =
    try {
        context.assets.list("books")?.size ?: 0
    } catch (ex: Exception) {
        0
    }

/**
 * Get identifier of the next book (if available)
 * @param context Context
 * @param currentId Current book identifier
 * @return next book identifier, or null if this is the last book
 */
fun getNextBook(context: Context, currentId: Int): Book? =
    try {
        loadBookMetadata(context, currentId + 1)
    } catch (ex: Exception) {
        null
    }

/**
 * Get identifier of the previous book (if available)
 * @param context Context
 * @param currentId Current book identifier
 * @return previous book identifier, or null if this is the first book
 */
fun getPreviousBook(context: Context, currentId: Int): Book? =
    try {
        loadBookMetadata(context, currentId - 1)
    } catch (ex: Exception) {
        null
    }


/**
 * Load title of the specified book
 * @param context Context
 * @param id Book identifier
 * @return Book metadata
 * @throws IOException in case loading fails
 */
@Throws(IOException::class)
fun loadBookMetadata(context: Context, id: Int) = Book(id).apply {
    context.assets.open(getMetadataPath(id)).use { s ->
        Properties().apply {
            load(s)
            title = getProperty("title", "")
            author = getProperty("author", "")
            chapters = generateChapters(getProperty("chapters", "1").toInt())
        }
    }
}

/**
 * Load list of books
 * @param context Context
 * @return list of books
 * @throws IOException in case loading fails
 */
@Throws(IOException::class)
fun loadBooks(context: Context) =
    (1..getBookCount(context))
        .map { loadBookMetadata(context, it) }
        .toList()

/**
 * Load text of the specified book
 * @param context Context
 * @param id Book identifier
 * @return List of lines
 * @throws IOException in case loading fails
 */
@Throws(IOException::class)
fun loadBookText(context: Context, id: Int): Array<List<String>> =
    context.assets.open(getTextPath(id)).bufferedReader().use { s ->
        s.readText()
            .split("###")
            .asSequence()
            .filter(String::isNotBlank)
            .map(String::trim)
            .map(String::lines)
            .toList()
            .toTypedArray()
    }

/**
 * Load font with specified name
 * @param context Context
 * @param name Font name
 * @return Generate typeface
 */
fun loadTypeface(context: Context, name: String, default: Typeface): Typeface =
    try {
        Typeface.createFromAsset(context.assets, getFontPath(name))
    } catch (ex: Exception) {
        default
    }


