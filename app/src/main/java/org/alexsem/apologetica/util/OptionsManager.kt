package org.alexsem.apologetica.util

import android.content.Context
import android.graphics.Color
import android.preference.PreferenceManager
import org.alexsem.apologetica.model.*

/**
 * Helper class to manage user preferences
 */
object OptionsManager {

    private val Context.preferences
        get() = PreferenceManager.getDefaultSharedPreferences(this)

    /**
     * Load main preferences into specific object
     * @param context Context
     * @return Populated [OptionsMain] object
     */
    fun loadSettings(context: Context): OptionsMain {
        val prefs = context.preferences
        return OptionsMain().apply {
            fc_back_color_day = prefs.getInt("fc_back_color_day", Color.parseColor("#cfbf9e"))
            fc_back_color_night = prefs.getInt("fc_back_color_night", Color.parseColor("#000000"))
            fc_text_color_day = prefs.getInt("fc_text_color_day", Color.parseColor("#000000"))
            fc_text_color_night = prefs.getInt("fc_text_color_night", Color.parseColor("#dddddd"))
            fc_text_color_cinnabar = prefs.getInt("fc_text_color_cinnabar", Color.parseColor("#6d4c41"))
            fc_other_color_service = prefs.getInt("fc_other_color_service", Color.parseColor("#6d4c41"))
            fc_text_face = prefs.getString("fc_text_face", "Roboto-Regular") ?: "Roboto-Regular"
            fc_text_bold = prefs.getString("fc_text_bold", "Roboto-Bold") ?: "Roboto-Bold"
            fc_text_italic = prefs.getString("fc_text_italic", "Roboto-Italic") ?: "Roboto-Italic"
            fc_text_title = prefs.getString("fc_text_title", "RobotoSlab-Bold") ?: "RobotoSlab-Bold"
            fc_text_size = prefs.getString("fc_text_size", "18")?.toFloat() ?: 18f
            fc_text_size *= context.resources.displayMetrics.density
            fc_text_spacing = prefs.getString("fc_text_spacing", "1.2")?.toFloat() ?: 1.2f
            orientation = prefs.getString("orientation", "0")?.toInt() ?: 0
            animation = prefs.getBoolean("animation", true)
            hyphenateMode = prefs.getString("hyphenate_mode", "2")?.toInt() ?: 2
            fillLine = prefs.getBoolean("fill_line", true)
            colPort = prefs.getString("columns_port", "1")?.toInt() ?: 1
            colLand = prefs.getString("columns_land", "2")?.toInt() ?: 1
        }
    }


    /**
     * Load quick actions into specific object
     * @param context Context
     * @return Populated [OptionsActions] object
     */
    fun loadActions(context: Context): OptionsActions {
        val prefs = context.preferences
        return OptionsActions().apply {
            actionsShort[ReaderZone.BOTTOM_LEFT] = QuickAction.getByCode(prefs.getInt("act_short_bl", 2))
            actionsShort[ReaderZone.BOTTOM] = QuickAction.getByCode(prefs.getInt("act_short_bc", 1))
            actionsShort[ReaderZone.BOTTOM_RIGHT] = QuickAction.getByCode(prefs.getInt("act_short_br", 1))
            actionsShort[ReaderZone.LEFT] = QuickAction.getByCode(prefs.getInt("act_short_ml", 2))
            actionsShort[ReaderZone.CENTER] = QuickAction.getByCode(prefs.getInt("act_short_mc", 13))
            actionsShort[ReaderZone.RIGHT] = QuickAction.getByCode(prefs.getInt("act_short_mr", 1))
            actionsShort[ReaderZone.TOP_LEFT] = QuickAction.getByCode(prefs.getInt("act_short_tl", 2))
            actionsShort[ReaderZone.TOP] = QuickAction.getByCode(prefs.getInt("act_short_tc", 2))
            actionsShort[ReaderZone.TOP_RIGHT] = QuickAction.getByCode(prefs.getInt("act_short_tr", 1))
            actionsLong[ReaderZone.BOTTOM_LEFT] = QuickAction.getByCode(prefs.getInt("act_long_bl", 2))
            actionsLong[ReaderZone.BOTTOM] = QuickAction.getByCode(prefs.getInt("act_long_bc", 1))
            actionsLong[ReaderZone.BOTTOM_RIGHT] = QuickAction.getByCode(prefs.getInt("act_long_br", 1))
            actionsLong[ReaderZone.LEFT] = QuickAction.getByCode(prefs.getInt("act_long_ml", 2))
            actionsLong[ReaderZone.CENTER] = QuickAction.getByCode(prefs.getInt("act_long_mc", 20))
            actionsLong[ReaderZone.RIGHT] = QuickAction.getByCode(prefs.getInt("act_long_mr", 1))
            actionsLong[ReaderZone.TOP_LEFT] = QuickAction.getByCode(prefs.getInt("act_long_tl", 2))
            actionsLong[ReaderZone.TOP] = QuickAction.getByCode(prefs.getInt("act_long_tc", 2))
            actionsLong[ReaderZone.TOP_RIGHT] = QuickAction.getByCode(prefs.getInt("act_long_tr", 1))
            actionsVolume[true] = QuickAction.getByCode(prefs.getInt("act_volume_up", 4))
            actionsVolume[false] = QuickAction.getByCode(prefs.getInt("act_volume_dn", 3))
        }
    }

    /**
     * Checks the initialization flag
     * @param context Context
     * @return true or false
     */
    fun isInitialized(context: Context): Boolean =
     context.preferences.contains("initialized")

    /**
     * Sets the initialization flag
     * @param context Context
     */
    fun setInitialized(context: Context) {
        context.preferences.edit().putBoolean("initialized", true).apply()
    }

    /**
     * Save new text size
     * @param context Context
     * @param newSize New font size
     */
    fun saveTextSize(context: Context, newSize: Float) {
        val size = Math.round(newSize / context.resources.displayMetrics.density).toString()
        context.preferences
            .edit()
            .putString("fc_text_size", size)
            .apply()
    }

    /**
     * Save identifier of currently opened book
     * @param context Context
     * @param bookId Book identifier
     */
    fun saveCurrentBookId(context: Context, bookId: Int) {
        context.preferences
            .edit()
            .putInt("current_book_id", bookId)
            .apply()
    }

    /**
     * Load identifier of book opened last
     * @param context Context
     * @return Book identifier
     */
    fun loadCurrentBookId(context: Context) =
        context.preferences.getInt("current_book_id", -1)

    /**
     * Save current position of the specified book
     * @param context Context
     * @param bookId Book identifier
     * @param position Current position to save
     */
    fun saveBookPosition(context: Context, bookId: Int, position: Position) {
        context.preferences
            .edit()
            .putString(String.format("book_position_%03d", bookId), position.toString())
            .apply()
    }

    /**
     * Load current position of the specified book
     * @param context Context
     * @param bookId Book identifier
     * @return Current position in the book
     */
    fun loadBookPosition(context: Context, bookId: Int) =
        Position(context.preferences.getString(String.format("book_position_%03d", bookId), "-1;-1;0") ?: "-1;-1;0")

    /**
     * Save action assigned to specific button
     * @param context Context
     * @param isLong  true for long press, false for regular press
     * @param zone    Reader zone
     * @param action  Action to assign
     */
    fun saveAction(context: Context, isLong: Boolean, zone: ReaderZone, action: QuickAction) {
        val zoneString: String = when (zone) {
            ReaderZone.TOP -> "tc"
            ReaderZone.TOP_RIGHT -> "tr"
            ReaderZone.RIGHT -> "mr"
            ReaderZone.BOTTOM_RIGHT -> "br"
            ReaderZone.BOTTOM -> "bc"
            ReaderZone.BOTTOM_LEFT -> "bl"
            ReaderZone.LEFT -> "ml"
            ReaderZone.TOP_LEFT -> "tl"
            ReaderZone.CENTER -> "mc"
        }
        val name = String.format("act_%s_%s", if (isLong) "long" else "short", zoneString)
        context.preferences
            .edit()
            .putInt(name, action.code)
            .apply()
    }

    /**
     * Save action assigned to specific volume buttons
     * @param context Context
     * @param isUp    true for "Volume Up", false for "Volume Down"
     * @param action  Action to assign
     */
    fun saveVolumeAction(context: Context, isUp: Boolean, action: QuickAction) {
        val name = String.format("act_volume_%s", if (isUp) "up" else "dn")
        context.preferences
            .edit()
            .putInt(name, action.code)
            .apply()
    }


}
