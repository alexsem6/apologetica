package org.alexsem.apologetica.util

/**
 * Class which is used for Russian words hyphenation
 */
class EsHyphenator(private val checkPrefixes: Boolean) : Hyphenator {

    private val MARK = "/"
    private val TYPE_x = "BCDFGHJKLMNÑPQRSTVWXZbcdfghjklmnñpqrstvwxz"
    private val TYPE_o = "AÁEÉIÍOÓUÚÜYaáeéiíoóuúüy"
    private val TYPE_i = ""
    private val RULES = arrayOf(
        arrayOf("ioo", "1"), arrayOf("iox", "1"), arrayOf("ixo", "1"), arrayOf("ixx", "1"), arrayOf("xooxo", "3"), //2
        arrayOf("oxxxxo", "3"), arrayOf("oxxxo", "2"), //3
        arrayOf("xoxo", "2"), arrayOf("oxxo", "2"), arrayOf("xooo", "2"), arrayOf("xoox", "2")
    )
    private val MAX_PREFIXES = 2
    private val PREFIX3 = sortedSetOf("In", "in")

    /**
     * Define the type of specified character
     * @param source Character which type to define
     * @return char with certain code value
     */
    private fun getLetterType(source: Char): Char = when {
        TYPE_x.contains(source) -> 'x'
        TYPE_o.contains(source) -> 'o'
        TYPE_i.contains(source) -> 'i'
        else -> '_'
    }

    /**
     * Perform hyphenation of the specified word
     * @param word Word to hyphenate
     * @return array of positions at which hyphenation marks may be inserted
     */
    override fun hyphenateWord(word: String): IntArray {
        var text = word
        val length = text.length
        if (length <= 3) { //Word too short
            return IntArray(0)
        }
        val prefixIdx = IntArray(MAX_PREFIXES)
        val appender = StringBuilder()

        val currentCode = StringBuilder()
        var currentPrefix = ""
        var vowels = 0
        var prefixes = 0
        for (i in 0 until length) {
            val c = text[i]
            val type = getLetterType(c)
            vowels += if (type == 'o') 1 else 0
            if (checkPrefixes) { //Need to check prefixes
                if (type != 'i' && prefixes < MAX_PREFIXES && PREFIX3.contains(currentPrefix)) {
                    prefixIdx[prefixes] = currentCode.length - prefixes
                    currentCode.append(MARK)
                    currentPrefix = ""
                    prefixes++
                }
                currentPrefix += c
            }
            currentCode.append(type)
        }
        var hyphenatedText = currentCode.toString()

        if (vowels <= 1) { //Only one syllable
            return IntArray(0)
        }

        if (checkPrefixes) {
            for ((offset, i) in (0 until prefixes).withIndex()) {
                val actualIndex = prefixIdx[i] + offset
                appender.setLength(0)
                text = appender
                    .append(text.substring(0, actualIndex))
                    .append(MARK)
                    .append(text.substring(actualIndex))
                    .toString()
            }
        }

        for (rule in RULES) {
            var index = hyphenatedText.indexOf(rule[0])
            while (index > -1) {
                val actualIndex = index + Integer.valueOf(rule[1])
                appender.setLength(0)
                hyphenatedText = appender
                    .append(hyphenatedText.substring(0, actualIndex))
                    .append(MARK)
                    .append(hyphenatedText.substring(actualIndex)).toString()
                appender.setLength(0)
                text = appender
                    .append(text.substring(0, actualIndex))
                    .append(MARK)
                    .append(text.substring(actualIndex))
                    .toString()
                index = hyphenatedText.indexOf(rule[0])
            }
        }

        val splitText = text.split(MARK.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val result = IntArray(splitText.size - 1)
        val size = result.size
        for (i in 0 until size) {
            result[i] = (if (i == 0) 0 else result[i - 1]) + splitText[i].length
        }
        return result
    }

}
