package org.alexsem.apologetica.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.*;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import org.alexsem.apologetica.R;
import org.alexsem.apologetica.model.Book;
import org.alexsem.apologetica.model.OptionsMain;
import org.alexsem.apologetica.model.Position;
import org.alexsem.apologetica.model.ReaderZone;
import org.alexsem.apologetica.util.Hyphenator;
import org.alexsem.apologetica.util.EsHyphenator;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.alexsem.apologetica.util.AssetLoaderKt.loadTypeface;

public class BookReader extends View {

    private static final int TOUCH_ACTION = 23542635;

    private final String DELIMITERS = "¡¿?!(),./[]:;‚„‘’“”–—•«¬­»·\n\r\t ";
    private final String HYPHENATOR = "-";
    private final int EPS_PAGINATOR = 3;
    private final float EPS_NEWLINE = 0.5f;
    private final int ANIM_LENGTH = 5;
    private int STATUS_HEIGHT = 10;
    private int STATUS_SCROLL = 8;
    private int STATUS_PADDING = 3;
    private int STATUS_SIZE = 14;
    private int INTERPAGE_PADDING = 5;

    private final float COEF_SIZE_CHAPTER = 1.2f;
    private final float COEF_SIZE_BOOK_TITLE = 1.85f;
    private final float COEF_SIZE_BOOK_AUTHOR = 1.2f;
    private final float COEF_SIZE_CONTENTS_HEADER = 1.1f;
    private final float COEF_SIZE_CONTENTS_ITEM = 0.8f;
    private final float COEF_SPACING = 2f;
    private final float COEF_SPACING_CONTENTS_HEADER = 1.8f;
    private final float COEF_SPACING_CONTENTS_FOOTER = 1.2f;
    private final float COEF_INTERSPACE_CONTENTS = 0.5f;
    private final float COEF_SPACE_SHRINK = 0.80f;

    private int FLING_VERT_THRESHOLD = 75;
    private int FLING_HORZ_MINIMAL = 100;

    private int BRIGHT_FRAME_WIDTH;
    private int BRIGHT_FRAME_HEIGHT;
    private int BRIGHT_FRAME_MARGIN = 25;
    private int BRIGHT_FRAME_PADDING = 20;
    private int BRIGHT_FRAME_THICK = 6;
    private float BRIGHT_VALUE_STEP = 0.086f;
    private float BRIGHT_VALUE_MIN = 0.055f;
    private float BRIGHT_VALUE_MAX = 1f;
    private int BRIGHT_ANIM_DURATION = 15;
    private int BRIGHT_FLING_STEP;
    private int BRIGHT_FLING_THRESH = 15;

    private int FONTSIZE_MIN;
    private int FONTSIZE_MAX;

    private Book book = null;
    private List<String>[] bookText = null;

    private Paint paint;
    private Paint conPaint;

    private Typeface regularFont;
    private Typeface headerFont;
    private Typeface boldFont;
    private Typeface italicFont;
    private String backWord;
    private String forwWord;
    private String waitWord;

    private int chapter = -1;
    private int page = 0;
    private int maxPages = 0;

    private int globalPage = 0;
    private int globalSize = 0;
    private int[] globalIndicies;

    private boolean contentsShowing = false;
    private int conPage = 0;
    private int conTotalPages = 0;
    private int conHeaderOffset = 0;
    private int conFooterOffset = 0;
    private int conButtonPressed = -2;
    private List<Position> contentPositions;
    private List<ContentButton> contentButtons;

    private UniversalHandler handler;
    private int touch = 0; //0 - not touched, 1 - next, >1 - already scrolled, -1 - previous, <-1 - already scrolled
    private LongTouchThread ltThread;
    private Bitmap bitmapStorage;
    private int animation = 0; //0 - no animation, >1 - next page, <-1 - previous page
    private AnimationThread anThread;
    private int brightnessTimeout;
    private BrightnessThread brightnessThread;
    private boolean scaleBegan = false;
    private float scaleFactor = 1f;
    private ScaleGestureDetector scaleGestureDetector;

    private float touchX;
    private float touchY;
    private ReaderZone touchZone;

    private boolean dictionaryEnabled = false;
    private boolean errorsEnabled = false;
    private List<WordButton> wordButtons;
    private int wordButtonPressed = -1;
    private OnWordLinkClickedListener dictionaryListener;
    private OnWordLinkClickedListener errorsListener;
    private OnTextSizeChangedListener textSizeListener;
    private OnQuickActionsListener quickActionListener;
    private OnBrightnessListener brightnessListener;
    private OnTranscendCurrentBookListener transcendCurrentBookListener;

    private List<Position> currentPositions;
    private SparseArray<SparseBooleanArray> currentHyphens;
    private SparseArray<SparseArray<Float>> currentSpacings;
    private BookCalculatorTask bookCalculator;

    private OptionsMain options;
    private Hyphenator hyphenator;
    private float brightness;
    private int time = 0;

    private int currentColumnCount = 1;

    public BookReader(Context context) {
        super(context);
        initialize(context);
    }

    public BookReader(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    //------------------------------------------------------------------------------------

    /**
     * Perform all necessary initializations
     *
     * @param context Context
     */
    private void initialize(Context context) {
        paint = generatePaint();
        conPaint = generatePaint();
        waitWord = context.getString(R.string.reader_wait);
        backWord = context.getString(R.string.reader_back);
        forwWord = context.getString(R.string.reader_forw);
        handler = new BookReader.UniversalHandler(this);
        contentButtons = new ArrayList<>();
        wordButtons = new ArrayList<>();
        int dpi = context.getResources().getDisplayMetrics().densityDpi;
        STATUS_HEIGHT = STATUS_HEIGHT * dpi / 160;
        STATUS_PADDING = STATUS_PADDING * dpi / 160;
        STATUS_SCROLL = STATUS_SCROLL * dpi / 160;
        STATUS_SIZE = STATUS_SIZE * dpi / 160;
        INTERPAGE_PADDING = INTERPAGE_PADDING * dpi / 160;
        FLING_VERT_THRESHOLD = FLING_VERT_THRESHOLD * dpi / 160;
        FLING_HORZ_MINIMAL = FLING_HORZ_MINIMAL * dpi / 160;
        BRIGHT_FRAME_MARGIN = BRIGHT_FRAME_MARGIN * dpi / 160;
        BRIGHT_FRAME_PADDING = BRIGHT_FRAME_PADDING * dpi / 160;
        BRIGHT_FRAME_THICK = BRIGHT_FRAME_THICK * dpi / 160;
        BRIGHT_FLING_THRESH = BRIGHT_FLING_THRESH * dpi / 160;
        FONTSIZE_MIN = getResources().getInteger(R.integer.picker_fontsize_min) * dpi / 160;
        FONTSIZE_MAX = getResources().getInteger(R.integer.picker_fontsize_max) * dpi / 160;
        this.setOnTouchListener(onTouchListener);
        scaleGestureDetector = new ScaleGestureDetector(context, onScaleGestureListener);
        brightness = (((Activity) context).getWindow()).getAttributes().screenBrightness;
        if (brightness < 0) {
            try {
                brightness = (float) Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS) / 255f;
            } catch (Settings.SettingNotFoundException ex) {
                brightness = 0.1f;
            }
        }
        Runnable run = new Runnable() {
            @Override
            public void run() {
                Calendar calendar = Calendar.getInstance();
                int ct = calendar.get(Calendar.HOUR_OF_DAY) * 60 + calendar.get(Calendar.MINUTE);
                if (time != ct) { //Time changed
                    time = ct;
                    BookReader.this.invalidate();
                }
                handler.postDelayed(this, 1000);
            }
        };
        handler.post(run);
    }


    //------------------------------------------------------------------------------------

    /**
     * Populate reader with book data
     *
     * @param book     Book metadata
     * @param bookText Book text
     * @param position Starting position
     */
    public void setData(Book book, List<String>[] bookText, Position position) {
        if (bookCalculator != null) {
            bookCalculator.cancel(true);
        }
        this.book = book;
        this.bookText = bookText;
        this.hyphenator = new EsHyphenator(options.hyphenateMode == 2);
        this.contentsShowing = false;
        requestPositionRelocation(position);
    }

    /**
     * Creates new paint with preset values
     *
     * @return Generated Paint object
     */
    private Paint generatePaint() {
        Paint result = new Paint();
        result.setAntiAlias(true);
        result.setTextAlign(Paint.Align.LEFT);
        result.setStrokeWidth(0f);
        return result;
    }

    //------------------------------------------------------------------------------------

    /**
     * Scroll text one screen up
     */
    private void calculatePreviousPage() {
        if (contentsShowing) { //Book contents
            if (conPage > 0) { //Can scroll
                conPage -= currentColumnCount;
                this.invalidate();
            }
        } else { //Regular text
            if (page > 0) { //Can scroll
                page -= currentColumnCount;
                globalPage -= currentColumnCount;
                this.invalidate();
            } else { //Not a chapter link
                if (chapter == 0) { //First chapter
                    calculateFirstPage();
                } else if (chapter > 0) { //Other chapter
                    CalculationResult result = calculate(book, bookText, chapter - 1, null, alwaysFalseCancelListener);
                    currentPositions = result.positions;
                    currentHyphens = result.hyphens;
                    currentSpacings = result.spacings;
                    chapter--;
                    maxPages = result.totalPages;
                    page = maxPages > 0 ? maxPages - currentColumnCount : 0;
                    globalPage -= currentColumnCount;
                    this.invalidate();
                }
            }
            if (dictionaryEnabled || errorsEnabled) { //Word links visible
                calculateWordButtons(wordButtons, currentHyphens, currentSpacings);
            }
        }
    }

    /**
     * Scroll text one screen down
     */
    private void calculateNextPage() {
        if (contentsShowing) { //Book contents
            if (conPage < conTotalPages - currentColumnCount) { //Can scroll
                conPage += currentColumnCount;
                this.invalidate();
            }
        } else { //Regular text
            if (page < maxPages - currentColumnCount) { //Can scroll
                page += currentColumnCount;
                globalPage += currentColumnCount;
                this.invalidate();
            } else { //Not a chapter link
                if (chapter < getChapterCount() - 1) { //Page can be shown
                    CalculationResult result = calculate(book, bookText, chapter + 1, null, alwaysFalseCancelListener);
                    currentPositions = result.positions;
                    currentHyphens = result.hyphens;
                    currentSpacings = result.spacings;
                    chapter += 1;
                    maxPages = result.totalPages;
                    page = 0;
                    globalPage += currentColumnCount;
                    this.invalidate();
                }
            }
            if (dictionaryEnabled || errorsEnabled) { //Word links visible
                calculateWordButtons(wordButtons, currentHyphens, currentSpacings);
            }
        }
    }

    /**
     * Go to the very first page
     */
    private void calculateFirstPage() {
        CalculationResult result = calculate(book, bookText, -1, null, alwaysFalseCancelListener);
        currentPositions = result.positions;
        currentHyphens = result.hyphens;
        currentSpacings = result.spacings;
        chapter = -1;
        page = 0;
        maxPages = 0;
        globalPage = 0;
        switchDictionary(false);
        switchErrors(false);
        this.invalidate();
    }

    /**
     * Go to the very last page
     */
    private void calculateLastPage() {
        CalculationResult result = calculate(book, bookText, getChapterCount() - 1, null, alwaysFalseCancelListener);
        currentPositions = result.positions;
        currentHyphens = result.hyphens;
        currentSpacings = result.spacings;
        chapter = getChapterCount() - 1;
        maxPages = result.totalPages;
        page = maxPages > 0 ? maxPages - currentColumnCount : 0;
        globalPage = globalSize > 0 ? globalSize - currentColumnCount : 0;
        switchDictionary(false);
        switchErrors(false);
        this.invalidate();
    }

    /**
     * Go to the beginning of specific chapter
     *
     * @param targetChapter Chapter to go to
     */
    private void calculateSpecificChapter(int targetChapter) {
        if (globalSize == 0 || (bookCalculator != null && !bookCalculator.isFinished())) {
            requestPositionRelocation(new Position(targetChapter));
        } else {
            CalculationResult result = calculate(book, bookText, targetChapter, null, alwaysFalseCancelListener);
            currentPositions = result.positions;
            currentHyphens = result.hyphens;
            currentSpacings = result.spacings;
            chapter = targetChapter;
            maxPages = result.totalPages;
            page = 0;
            globalPage = targetChapter > -1 ? (globalIndicies != null ? globalIndicies[targetChapter] : 0) : 0;
            switchDictionary(false);
            switchErrors(false);
            this.invalidate();
        }
    }

    //------------------------------------------------------------------------------------

    /**
     * Navigate to next page with step
     *
     * @param step Number of pages to skip
     */
    public void goToNextPage(int step) {
        if (chapter >= getChapterCount() - 1 && page >= maxPages - currentColumnCount && transcendCurrentBookListener != null) { //End of book
            transcendCurrentBookListener.onGoToNextBook();
            return;
        }
        for (int i = 0; i < step; i++) {
            if ((chapter < getChapterCount() - 1) || page < maxPages - currentColumnCount) { //Page can be shown
                animation = 1;
                startAnimationThread();
            }
            calculateNextPage();
        }
    }

    /**
     * Navigate to previous page with step
     *
     * @param step Number of pages to skip
     */
    public void goToPreviousPage(int step) {
        if (chapter == -1 && page == 0 && transcendCurrentBookListener != null) { //End of book
            transcendCurrentBookListener.onGoToPreviousBook();
            return;
        }

        for (int i = 0; i < step; i++) {
            if ((chapter > -1) || page > 0) { //Page can be shown
                animation = -1;
                startAnimationThread();
            }
            calculatePreviousPage();
        }
    }


    /**
     * Navigate to next contents page
     */
    public void goToContentsNextPage() {
        if (conPage < conTotalPages - currentColumnCount) {
            storeCurrentBitmap();
            animation = 1;
            startAnimationThread();
            calculateNextPage();
        }
    }

    /**
     * Navigate to previous contents page
     */
    public void goToContentsPreviousPage() {
        if (conPage > 0) {
            storeCurrentBitmap();
            animation = -1;
            startAnimationThread();
            calculatePreviousPage();
        }
    }

    /**
     * Navigate to next chapter
     */
    public void goToNextChapter() {
        if (chapter >= getChapterCount() - 1 && transcendCurrentBookListener != null) { //End of book
            transcendCurrentBookListener.onGoToNextBook();
            return;
        }
        if (chapter == getChapterCount() - 1 && globalPage < globalSize - currentColumnCount) { //Last chapter (go to last page)
            storeCurrentBitmap();
            animation = 1;
            startAnimationThread();
            calculateLastPage();
        } else if (chapter < getChapterCount() - 1) { //Go to next chapter
            storeCurrentBitmap();
            animation = 1;
            calculateSpecificChapter(chapter + 1);
            startAnimationThread();
        }
    }

    /**
     * Navigate to previous chapter
     */
    public void goToPreviousChapter() {
        if (chapter == -1 && transcendCurrentBookListener != null) { //End of book
            transcendCurrentBookListener.onGoToPreviousBook();
            return;
        }
        if (page > 0) { //Middle of chapter (go to the beginning of chapter)
            storeCurrentBitmap();
            animation = -1;
            startAnimationThread();
            globalPage -= page;
            page = 0;
            this.invalidate();
            switchDictionary(false);
            switchErrors(false);
        } else if (chapter > -1) { //Go to previous chapter
            storeCurrentBitmap();
            animation = -1;
            calculateSpecificChapter(chapter - 1);
            startAnimationThread();
        }
    }

    /**
     * Navigate to the very first page
     */
    public void goToFirstPage() {
        boolean animate = globalPage > 0;
        calculateFirstPage();
        if (animate) {
            storeCurrentBitmap();
            animation = -1;
            startAnimationThread();
        }
    }

    /**
     * Navigate to the very last page
     */
    public void goToLastPage() {
        boolean animate = globalPage < globalSize - currentColumnCount;
        calculateLastPage();
        if (animate) {
            storeCurrentBitmap();
            animation = 1;
            startAnimationThread();
        }
    }

    //----------------------------------------------------------------------------------

    /**
     * Increase brightness one step (if possible)
     */
    public void increaseBrightness() {
        changeBrightness(Math.min(BRIGHT_VALUE_MAX, brightness + BRIGHT_VALUE_STEP), true);
    }

    /**
     * Decrease brightness one step (if possible)
     */
    public void decreaseBrightness() {
        changeBrightness(Math.max(BRIGHT_VALUE_MIN, brightness - BRIGHT_VALUE_STEP), true);
    }

    /**
     * Changes brightness level and initiates showing brightness frame
     *
     * @param newValue    New brightness value
     * @param startThread true to start brightness frame animation thread
     */
    private void changeBrightness(float newValue, boolean startThread) {
        brightness = newValue;
        if (brightnessListener != null) {
            brightnessListener.onBrightnessChanged(newValue);
        }
        if (startThread) {
            brightnessTimeout = BRIGHT_ANIM_DURATION;
            if (brightnessThread == null || !brightnessThread.isRunning()) {
                brightnessThread = new BrightnessThread();
                brightnessThread.start();
            }
        }
        this.invalidate();
    }

    //------------------------------------------------------------------------------------

    /**
     * Enables or disables displaying of contents
     *
     * @param to true if contents must be shown, false if text must be shown
     */
    public void switchContents(boolean to) {
        if (to) { //Show book contents
            if (book != null) {
                contentsShowing = true;
                Position position = new Position(-2, chapter < 0 ? 0 : chapter, 0);
                CalculationResult result = calculateContents(position, contentButtons);
                contentPositions = result.positions;
                conTotalPages = result.totalPages;
                conPage = result.targetPage;
                conButtonPressed = -2;
            }
        } else { //Hide book contents
            contentsShowing = false;
        }
        this.invalidate();
    }

    /**
     * Enables or disables displaying dictionary links
     *
     * @param to true if dictionary links must be shown, false if regular text
     *           must be shown
     */
    public void switchDictionary(boolean to) {
        if (to && canShowWordLinks()) { //Show dictionary links
            errorsEnabled = false;
            dictionaryEnabled = true;
            calculateWordButtons(wordButtons, currentHyphens, currentSpacings);
        } else { //Hide dictionary links
            dictionaryEnabled = false;
        }
        this.invalidate();
    }

    /**
     * Enables or disables displaying error links
     *
     * @param to true if error links must be shown, false if regular text must be
     *           shown
     */
    public void switchErrors(boolean to) {
        if (to && canShowWordLinks()) { //Show error links
            dictionaryEnabled = false;
            errorsEnabled = true;
            calculateWordButtons(wordButtons, currentHyphens, currentSpacings);
        } else { //Hide error links
            errorsEnabled = false;
        }
        this.invalidate();
    }

    /**
     * Defines whether word button links (either dictionary lookup or mistakes in text) may be safely shown
     * Happens when not viewing contents and not on the book title page
     *
     * @return true or false
     */
    public boolean canShowWordLinks() {
        return !contentsShowing && getCurrentChapter() > -1;
    }

    //------------------------------------------------------------------------------------

    /**
     * Define number of screen "section" to which specified dot belongs
     *
     * @param x Dot's X-axis
     * @param y Dot's Y-axis
     * @return number of section (from 1 to 9 place the same as numpad keys)
     */
    private ReaderZone getZoneForCoords(float x, float y) {
        int zone = (2 - (int) y / (this.getHeight() / 3)) * 3 + (int) x / (this.getWidth() / 3) + 1;
        switch (zone) {
            case 1:
                return ReaderZone.BOTTOM_LEFT;
            case 2:
                return ReaderZone.BOTTOM;
            case 3:
                return ReaderZone.BOTTOM_RIGHT;
            case 4:
                return ReaderZone.LEFT;
            case 5:
                return ReaderZone.CENTER;
            case 6:
                return ReaderZone.RIGHT;
            case 7:
                return ReaderZone.TOP_LEFT;
            case 8:
                return ReaderZone.TOP;
            case 9:
                return ReaderZone.TOP_RIGHT;
            default:
                return null;
        }
    }

    /**
     * Returns number of chapters
     *
     * @return Book size
     */
    private int getChapterCount() {
        return book == null ? -1 : book.getChapters().size();
    }

    /**
     * Returns current chapter number
     *
     * @return Chapter number
     */
    public int getCurrentChapter() {
        return chapter;
    }

    /**
     * Returns current position of currently viewed book
     *
     * @return Current position in book
     */
    public Position getCurrentPosition() {
        return (currentPositions == null || page < 0 || page > currentPositions.size() - currentColumnCount) ? null : currentPositions.get(page);
    }

    public boolean isContentsShowing() {
        return contentsShowing;
    }

    public boolean isDictionaryEnabled() {
        return dictionaryEnabled;
    }

    public boolean isErrorsEnabled() {
        return errorsEnabled;
    }

    //------------------------------------------------------------------------------------

    public void setOptions(OptionsMain options, Typeface typeface) {
        this.options = options;
        this.regularFont = typeface;
        this.hyphenator = new EsHyphenator(options.hyphenateMode == 2);
        this.headerFont = loadTypeface(getContext(), options.fc_text_title, Typeface.DEFAULT_BOLD);
        this.regularFont = loadTypeface(getContext(), options.fc_text_face, Typeface.DEFAULT);
        this.boldFont = loadTypeface(getContext(), options.fc_text_bold, Typeface.DEFAULT_BOLD);
        this.italicFont = loadTypeface(getContext(), options.fc_text_italic, Typeface.DEFAULT);
        requestPositionRelocation(null);
    }

    public void setDictionaryListener(OnWordLinkClickedListener dictionaryListener) {
        this.dictionaryListener = dictionaryListener;
    }

    public void setErrorsListener(OnWordLinkClickedListener errorsListener) {
        this.errorsListener = errorsListener;
    }

    public void setTextSizeListener(OnTextSizeChangedListener textSizeListener) {
        this.textSizeListener = textSizeListener;
    }

    public void setQuickActionListener(OnQuickActionsListener quickActionListener) {
        this.quickActionListener = quickActionListener;
    }

    public void setBrightnessListener(OnBrightnessListener brightnessListener) {
        this.brightnessListener = brightnessListener;
    }

    public void setTranscendCurrentBookListener(OnTranscendCurrentBookListener transcendCurrentBookListener) {
        this.transcendCurrentBookListener = transcendCurrentBookListener;
    }

    //------------------------------------------------------------------------------------

    /**
     * Perform calculations for specified chapter
     *
     * @param book     Book to calculate for
     * @param bookText Text of the book to calculate for
     * @param chapter  Chapter to calculate
     * @param target   Position to find (if need to)
     * @param cancel   Listener for calculation cancellation
     * @return Calculation results
     */
    private CalculationResult calculate(Book book, List<String>[] bookText, int chapter, Position target, BookCalculatorCancelListener cancel) {
        int canvasHeight = this.getHeight() - STATUS_HEIGHT;
        float canvasWidth = this.getWidth();
        Paint p = generatePaint();

        CalculationResult result = new CalculationResult();
        if (book == null || bookText == null || chapter < -1 || chapter > bookText.length - 1) {
            return result;
        }

        int y = 0;
        String line;

        if (cancel.isCancelled()) {
            return result;
        }

        if (chapter < 0) { //Book title
            result.targetPage = 0;
            result.totalPages = currentColumnCount;
            for (int i = 0; i < currentColumnCount; i++) {
                result.positions.add(new Position(-1));
            }
            return result;
        } else { //Regular chapter text
            p.setTextSize(options.fc_text_size);
            p.setTypeface(regularFont);

            line = book.getChapters().get(chapter).getTitle();

            if (line != null && line.trim().length() > 0) { //Null check
                p.setTextSize(p.getTextSize() * COEF_SIZE_CHAPTER);
                float size = p.getTextSize();
                float spacing = size * COEF_SPACING;
                float spaceWidth = p.measureText(" ");
                boolean wordsPresent = false;
                int x = 0;
                int index = 0;
                for (String token : line.split(" ")) {
                    float width = p.measureText(token);
                    if (wordsPresent && x + width > canvasWidth) { //Need to wrap text to next line
                        wordsPresent = false;
                        y += size * options.fc_text_spacing;
                        x = 0;
                    }
                    if (x == 0 && (y + spacing) % canvasHeight < spacing) { //Need to implement interpage spacing
                        y += canvasHeight - y % canvasHeight;
                    }
                    if (x == 0 && Math.abs(y - canvasHeight) % canvasHeight < EPS_PAGINATOR) { //Start of the page
                        result.positions.add(new Position(chapter, -1, index));
                    }
                    if (target != null && target.checkPosition(chapter, -1, index)) { //Found target position of text
                        result.targetPage = result.positions.size() - 1;
                    }
                    wordsPresent = true;
                    x += width + spaceWidth;
                    index++;
                }
                y += spacing;
            }

            canvasWidth = this.getWidth() / currentColumnCount - INTERPAGE_PADDING * (currentColumnCount - 1);
            int titleHeight = y;

            if (cancel.isCancelled()) {
                return result;
            }

            //------------------------------
            List<String> chapterText = bookText[chapter];
            for (int l = 0; l < chapterText.size(); l++) {

                if (cancel.isCancelled()) {
                    return result;
                }

                line = chapterText.get(l);
                p.setTypeface(regularFont);
                p.setTextSize(options.fc_text_size);
                float x = 0f;
                float size = p.getTextSize();
                float spacing = size * COEF_SPACING;

                int bLeftSpaces = -1;
                int bRightSpaces = -1;
                float bLeft = 0;
                float bRight = 0;
                float bTop = 0;
                float bBottom = 0;

                float hyphenWidth = p.measureText(HYPHENATOR);
                float spaceWidthDefault = p.measureText(" ") * COEF_SPACE_SHRINK;
                float[] charWidths = new float[line.length()];
                p.getTextWidths(line, charWidths);
                boolean wordStart = true;
                boolean bracket = false;
                boolean bracket2 = false;
                int hyphenateAt = -1;
                int lineStartsAt = -1;
                int spacesInLine = 0;

                for (int index = 0; index < line.length(); index++) { //For all characters
                    if (options.fillLine && index == 2) { //First character in any line
                        lineStartsAt = 2;
                        spacesInLine = 0;
                        bLeftSpaces = -1;
                        bRightSpaces = -1;
                        result.setSpacing(l, index, spaceWidthDefault);
                    }
                    switch (line.charAt(index)) {
                        case '[':
                            bracket = true;
                            p.setTypeface(boldFont);
                            continue;
                        case ']':
                            p.setTypeface(regularFont);
                            continue;
                        case '{':
                            bracket2 = true;
                            p.setTypeface(italicFont);
                            continue;
                        case '}':
                            p.setTypeface(regularFont);
                            continue;
                        case ' ':
                            spacesInLine++;
                            bRightSpaces++;
                            break;
                    }
                    float width = charWidths[index];
                    if (hyphenateAt == index) { //Need to add hyphenation mark right here
                        y += size * options.fc_text_spacing;
                        bRight = x + 2;
                        bRight += hyphenWidth;
                        if (options.fillLine) {
                            float newSpacing = result.spacings.get(l).get(lineStartsAt);
                            if (bLeftSpaces > -1) { //Need to adjust left bound of current link button
                                bLeft += (newSpacing - spaceWidthDefault) * bLeftSpaces;
                                bLeftSpaces = -1;
                            }
                            bRight = bRight >= canvasWidth ? canvasWidth - 1 : bRight;
                            if (bRightSpaces > -1) { //Need to adjust right bound of current link button
                                bRight += (newSpacing - spaceWidthDefault) * bRightSpaces;
                                bRightSpaces = -1;
                            }
                        }
                        x = 0f;
                        result.addHyphen(l, index - (bracket ? 1 : 0) - (bracket2 ? 1 : 0), true);
                        hyphenateAt = -1;
                        lineStartsAt = index;
                        spacesInLine = 0;
                    }
                    if (wordStart) { //Start of the word
                        hyphenateAt = -1;
                        int f = line.indexOf(' ', index);
                        int wordWidth = Math.round(sumFloats(charWidths, index, f > -1 ? f : line.length()));
                        if (x + wordWidth > canvasWidth) { //Need to wrap word to next line
                            if (options.hyphenateMode > 0) { //Need to try to hyphenate word
                                float spaceLeft = canvasWidth - x;
                                if (spaceLeft > 2 * hyphenWidth) { //Some space left
                                    String word = line.substring(index, f > -1 ? f : line.length());
                                    int[] hyphenPositions = hyphenator.hyphenateWord(word);
                                    for (int h = hyphenPositions.length - 1; h >= 0; h--) {
                                        int pos = hyphenPositions[h];
                                        float sylWidth = p.measureText(word.substring(0, pos)) + hyphenWidth;
                                        if (sylWidth <= spaceLeft) {
                                            hyphenateAt = index + pos;
                                            if (options.fillLine) {
                                                float newSpacing = spaceWidthDefault + (spacesInLine > 0 ? (spaceLeft - 1f - sylWidth) / (float) spacesInLine : 0f);
                                                result.setSpacing(l, lineStartsAt, newSpacing);
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            if (hyphenateAt < 0) { //Hyphenation not defined (regular wrap)
                                y += size * options.fc_text_spacing;
                                bRight = x + 2;
                                float offsetX = (this.getWidth() / currentColumnCount) * ((result.positions.size() - 1) % currentColumnCount) + ((result.positions.size() - 1) % currentColumnCount > 0 ? INTERPAGE_PADDING : 0);
                                bRight = Math.min(bRight, canvasWidth + offsetX);
                                result.addHyphen(l, index - (bracket ? 1 : 0) - (bracket2 ? 1 : 0), false);
                                if (options.fillLine) {
                                    float newSpacing = spaceWidthDefault + (spacesInLine > 1 ? (canvasWidth - x - 1f + spaceWidthDefault) / (spacesInLine - 1f) : 0f);
                                    result.setSpacing(l, lineStartsAt, newSpacing);
                                    if (bLeftSpaces > -1) { //Need to adjust left bound of current link button
                                        bLeft += (newSpacing - spaceWidthDefault) * bLeftSpaces;
                                        bLeftSpaces = -1;
                                    }
                                    if (bRightSpaces > -1) { //Need to adjust right bound of current link button
                                        bRight += (newSpacing - spaceWidthDefault) * bRightSpaces;
                                        bRightSpaces = -1;
                                    }
                                }
                                lineStartsAt = index;
                                spacesInLine = 0;
                                x = 0f;
                            }
                        }
                        wordStart = false;
                    }
                    if (Math.abs(x) < EPS_NEWLINE && (y + spacing) % canvasHeight < spacing) { //Need to implement interpage spacing
                        y += canvasHeight - y % canvasHeight;
                    }
                    if (Math.abs(x) < EPS_NEWLINE && Math.abs(y - canvasHeight) % canvasHeight < EPS_PAGINATOR) { //Start of the page
                        if (result.positions.size() % currentColumnCount > 0 && result.positions.get(result.positions.size() - (result.positions.size() % currentColumnCount)).getLineNumber() < 0) {
                            y += (titleHeight % canvasHeight);
                        }
                        result.positions.add(new Position(chapter, l, index));
                    }

                    float offsetX = (this.getWidth() / currentColumnCount) * ((result.positions.size() - 1) % currentColumnCount) + ((result.positions.size() - 1) % currentColumnCount > 0 ? INTERPAGE_PADDING : 0);

                    if (target != null && target.checkPosition(chapter, l, index)) { //Found target position of text
                        result.targetPage = result.positions.size() - 1;
                    }
                    if (options.fillLine && index >= 2 && line.charAt(index) == ' ') { //Space character
                        x += spaceWidthDefault;
                    } else { //Any other character
                        x += width;
                    }
                    if (line.charAt(index) == ' ') { //Need to start new word
                        wordStart = true;
                    }
                    bracket = false;
                    bracket2 = false;
                }
                float offsetX = (this.getWidth() / currentColumnCount) * ((result.positions.size() - 1) % currentColumnCount) + ((result.positions.size() - 1) % currentColumnCount > 0 ? INTERPAGE_PADDING : 0);
                if (options.fillLine && lineStartsAt > -1) { //Adjustments to the last line
                    float newSpacing = Math.min(spaceWidthDefault / COEF_SPACE_SHRINK, spaceWidthDefault + (spacesInLine > 0 ? (canvasWidth - x) / (spacesInLine) : 0f));
                    result.setSpacing(l, lineStartsAt, newSpacing);
                }
                y += spacing;
            }
            result.totalPages = result.positions.size();
        }

        if (cancel.isCancelled()) {
            return result;
        }

        while (result.totalPages % currentColumnCount > 0) { //Add blank pages in multi-column layout
            result.totalPages++;
            String tempLine = bookText[chapter].get(bookText[chapter].size() - 1);
            result.positions.add(new Position(chapter, bookText[chapter].size() - 1, tempLine.length()));
        }
        if (result.targetPage % currentColumnCount > 0) {
            result.targetPage--;
        }
        return result;
    }

    //------------------------------------------------------------------------------------

    /**
     * Perform calculations for book contents
     *
     * @param target Position to find (if need to)
     * @return Calculation results
     */
    private CalculationResult calculateContents(Position target, List<ContentButton> buttons) {
        int canvasHeight = this.getHeight();
        int canvasWidth = this.getWidth();
        Paint p = generatePaint();

        CalculationResult result = new CalculationResult();
        buttons.clear();
        if (book == null) {
            return result;
        }
        int y = 0;
        int lX = 0;
        conHeaderOffset = 0;
        conFooterOffset = 0;
        String line;

        p.setTextSize(options.fc_text_size);
        p.setTypeface(Typeface.DEFAULT);
        line = book.getTitle();

        if (line.trim().length() > 0) { //Null check
            float spacing = p.getFontSpacing() * COEF_SPACING_CONTENTS_HEADER;
            float size = p.getTextSize() * COEF_SIZE_CONTENTS_HEADER;
            float spaceWidth = p.measureText(" ");
            boolean wordsPresent = false;
            int x = lX;
            for (String token : line.split(" ")) {
                float width = p.measureText(token);
                if (wordsPresent && x + width > canvasWidth) { //Need to wrap text to next line
                    wordsPresent = false;
                    y += size;
                    x = lX;
                }
                wordsPresent = true;
                x += width + spaceWidth;
            }
            y += spacing;

            conHeaderOffset = y; //Book specific
            canvasHeight -= conHeaderOffset;
            y = 0;
        }

        canvasWidth = this.getWidth() / currentColumnCount - INTERPAGE_PADDING * (currentColumnCount - 1);

        //------------------------------
        p.setTextSize(options.fc_text_size * COEF_SIZE_CONTENTS_ITEM);
        float spacing = p.getFontSpacing();
        float size = p.getTextSize();
        float spaceWidth = p.measureText(" ");
        int interspace = Math.round(size * COEF_INTERSPACE_CONTENTS);

        canvasHeight -= interspace;

        //--- Measure title sizes ---
        int[] sizes = new int[book.getChapters().size()];
        for (int l = 0; l < book.getChapters().size(); l++) {
            sizes[l] = 1;
            line = "  " + book.getChapters().get(l).getTitle();
            boolean wordsPresent = false;
            int x = lX;
            for (String token : line.split(" ")) {
                float width = p.measureText(token);
                if (wordsPresent && x + width > canvasWidth) { //Need to wrap text to next line
                    wordsPresent = false;
                    x = lX;
                    sizes[l]++;
                }
                wordsPresent = true;
                x += width + spaceWidth;
            }
        }
        //------------

        int totalVertSize = getSizesSum(sumInts(sizes), size) + (sizes.length - 1) * interspace;
        if (totalVertSize < canvasHeight) { //All items fit on 1 screen
            result.totalPages = currentColumnCount;
            result.targetPage = 0;
            result.positions.add(new Position(-2, 0, 0));
            for (int s : sizes) {
                int bl = 1;
                int br = (this.getWidth() / currentColumnCount) - 1;
                int bt = y + conHeaderOffset + interspace - 2;
                int bb = y + getSizesSum(s, size) + interspace + conHeaderOffset + interspace;
                buttons.add(new ContentButton(0, bl, br, bt, bb));
                y += getSizesSum(s, size);
                y += interspace;
            }
        } else { //Split to several screens
            conFooterOffset += (totalVertSize >= canvasHeight * currentColumnCount) ? spacing * COEF_SPACING_CONTENTS_FOOTER : 0;
            canvasHeight -= conFooterOffset; //Navigation buttons must fit
            for (int t = 0; t < sizes.length; t++) {
                if ((y + getSizesSum(sizes[t], size) + interspace) % canvasHeight < getSizesSum(sizes[t], size) + interspace) { //Need to implement interpage spacing
                    y += canvasHeight - y % canvasHeight;
                }
                if (Math.abs(y - canvasHeight) % canvasHeight < EPS_PAGINATOR) { //Start of the page
                    result.positions.add(new Position(-2, t, 0));
                }
                if (target != null && target.checkPosition(-2, t, 0)) { //Found target position of text
                    result.targetPage = result.positions.size() - 1;
                }
                int bp = result.positions.size() - 1 - (result.positions.size() - 1) % currentColumnCount;
                int bl = ((result.positions.size() - 1) % currentColumnCount) * (this.getWidth() / currentColumnCount) + 1;
                int br = ((result.positions.size() - 1) % currentColumnCount + 1) * (this.getWidth() / currentColumnCount) - 1;
                int bt = y % canvasHeight + conHeaderOffset + interspace - 2;
                int bb = y % canvasHeight + getSizesSum(sizes[t], size) + interspace + conHeaderOffset + interspace;
                buttons.add(new ContentButton(bp, bl, br, bt, bb));
                y += getSizesSum(sizes[t], size);
                y += interspace;
            }
        }
        result.totalPages = result.positions.size();
        while (result.totalPages % currentColumnCount > 0) {
            result.totalPages++;
            result.positions.add(new Position(-2, sizes.length, 0));
        }
        if (result.targetPage % currentColumnCount > 0) {
            result.targetPage--;
        }

        return result;
    }

    //------------------------------------------------------------------------------------

    /**
     * Utility methods to get links
     *
     * @param buttons Buttons array to populate
     * @param line    Chapter title line
     * @param x       Starting X-axis position
     * @param y       Starting Y-axis position
     * @param spacing Current font spacing
     */
    private void calculateTitleLinks(List<WordButton> buttons, String line, int x, int y, float spacing) {
        StringBuilder linkWord = new StringBuilder();
        boolean buttonStart = true;
        int bLeft = 0;
        int bRight;
        int bTop = 0;
        int bBottom = 0;

        float widths[] = new float[line.length()];
        paint.getTextWidths(line, widths);
        for (int i = 0; i < line.length(); i++) {
            char ch = line.charAt(i);
            if (ch == '[' || ch == ']' || ch == '{' || ch == '}') {
                continue;
            }
            if (buttonStart && DELIMITERS.indexOf(ch) == -1) { //Start of button
                buttonStart = false;
                bLeft = (x > 0 ? x - 3 : 0);
                bTop = y;
                bBottom = Math.round(y + spacing);
                linkWord.setLength(0);
            } else if (!buttonStart && DELIMITERS.indexOf(ch) > -1) { //End of button
                buttonStart = true;
                bRight = x + 2;
                buttons.add(new WordButton(linkWord.toString(), -1, 0, bLeft, bRight, bTop, bBottom));
            }
            if (!buttonStart) {
                linkWord.append(ch);
            }
            x += widths[i];
        }
        if (!buttonStart) { //Button still needs to be added
            bRight = x + 2;
            buttons.add(new WordButton(linkWord.toString(), -1, 0, bLeft, bRight, bTop, bBottom));
        }
    }

    /**
     * Calculate positions of word links
     *
     * @param buttons      Buttons array to populate
     * @param lineHyphens  List of hyphenations and wraps for reference
     * @param wordSpacings List of word spacings for reference
     */
    private void calculateWordButtons(List<WordButton> buttons, SparseArray<SparseBooleanArray> lineHyphens, SparseArray<SparseArray<Float>> wordSpacings) {
        float canvasWidth = this.getWidth();
        int y = 0;
        paint.setTypeface(regularFont);

        buttons.clear();
        if (chapter < 0 || chapter > book.getChapters().size() - 1) { //Out of bounds
            return;
        }

        StringBuilder linkWord = new StringBuilder();
        String line = book.getChapters().get(chapter).getTitle();
        if (line != null && line.trim().length() > 0 && currentPositions.get(page).getLineNumber() == -1) { //Do not need to print
            paint.setTextSize(options.fc_text_size * COEF_SIZE_CHAPTER);

            float size = paint.getTextSize();
            float spacing = size * COEF_SPACING;
            float spaceWidth = paint.measureText(" ");

            StringBuilder builder = new StringBuilder();
            float x = 0f;
            String[] words = line.split(" "); //Words contained in title
            int startIndex = currentPositions.get(page).getCharIndex();
            int endIndex = (page < currentPositions.size() - 1 && currentPositions.get(page + 1).getLineNumber() == -1) ? currentPositions.get(page + 1)
                    .getCharIndex() : words.length;
            for (int index = startIndex; index < endIndex; index++) {
                String token = words[index];
                float width = paint.measureText(token);
                if (builder.length() > 0 && x + width > canvasWidth) { //Need to wrap text to next line
                    calculateTitleLinks(buttons, builder.toString().trim(), Math.round((canvasWidth - x + spaceWidth) / 2), y, spacing);
                    builder.setLength(0);
                    y += size * options.fc_text_spacing;
                    x = 0f;
                }
                builder.append(token).append(' ');
                x += width + spaceWidth;
            }
            if (builder.length() > 0) { //Need to print rest of the title
                calculateTitleLinks(buttons, builder.toString().trim(), Math.round((canvasWidth - x + spaceWidth) / 2), y, spacing);
            }
            y += spacing;
        }
        //---------------------------

        canvasWidth = this.getWidth() / currentColumnCount - INTERPAGE_PADDING * (currentColumnCount - 1);

        int storeY = y;
        for (int p = 0; p < currentColumnCount; p++) {
            int offsetX = (this.getWidth() / currentColumnCount) * (p % currentColumnCount) + (p % currentColumnCount > 0 ? INTERPAGE_PADDING : 0);
            y = storeY;

            List<String> text = bookText[chapter];
            int startLine = (currentPositions.get(page + p).getLineNumber() > -1) ? currentPositions.get(page + p).getLineNumber() : 0;
            int endLine = (page + p < currentPositions.size() - 1 && currentPositions.get(page + p + 1).getLineNumber() > -1) ? currentPositions.get(
                    page + p + 1).getLineNumber() + 1 : text.size();
            for (int l = startLine; l < endLine; l++) {
                paint.setTextSize(options.fc_text_size);
                SparseBooleanArray hyphens = lineHyphens.get(l);
                SparseArray<Float> spacings = wordSpacings.get(l);

                line = text.get(l);
                int x = 0;

                float size = paint.getTextSize();
                float spacing = size * COEF_SPACING;
                float spaceWidth = paint.measureText(" ");

                float[] charWidths = new float[line.length()];
                paint.getTextWidths(line, charWidths);

                boolean buttonStart = true;
                int bIndex = 0;
                float bLeft = 0;
                float bRight;
                float bTop = 0;
                float bBottom = 0;
                int startIndex = (currentPositions.get(page + p).getLineNumber() == l) ? currentPositions.get(page + p).getCharIndex() : 0;
                int endIndex = (page + p < currentPositions.size() - 1 && currentPositions.get(page + p + 1).getLineNumber() == l) ? currentPositions.get(
                        page + p + 1).getCharIndex() : line.length();

                if (startIndex > 0) { //Need to recreate missing part of previously hyphenated word
                    buttonStart = false;
                    bIndex = startIndex;
                    bLeft = (x > 0 ? x - 3 : 0) + offsetX;
                    bTop = y;
                    bBottom = Math.round(y + spacing);
                    linkWord.setLength(0);
                    while (DELIMITERS.indexOf(line.charAt(bIndex - 1)) == -1) {
                        linkWord.insert(0, line.charAt(bIndex - 1));
                        bIndex--;
                        if (bIndex == 0)
                            break;
                    }
                }

                for (int index = startIndex; index < endIndex; index++) { //For all characters
                    if (index >= line.length()) { //Out of bounds
                        continue;
                    }
                    if (options.fillLine && spacings != null && spacings.indexOfKey(index) > -1) { //Word spacing needs to be adjusted
                        spaceWidth = spacings.get(index);
                    }
                    char ch = line.charAt(index);
                    if (ch == '[' || ch == ']' || ch == '{' || ch == '}') {
                        continue;
                    }
                    float width = charWidths[index];

                    if (buttonStart && DELIMITERS.indexOf(ch) == -1) { //Start of button
                        buttonStart = false;
                        bIndex = index;
                        bLeft = (x > 0 ? x - 3 : 0) + offsetX;
                        bTop = y;
                        bBottom = Math.round(y + spacing);
                        linkWord.setLength(0);
                    } else if (!buttonStart && DELIMITERS.indexOf(ch) > -1) { //End of button
                        buttonStart = true;
                        bRight = (x > 0 ? x + 2 : canvasWidth) + offsetX;
                        buttons.add(new WordButton(linkWord.toString(), l, bIndex, bLeft, bRight, bTop, bBottom));
                    }
                    if (!buttonStart) {
                        linkWord.append(ch);
                    }

                    if (options.fillLine && ch == ' ') {
                        x += spaceWidth;
                    } else {
                        x += width;
                    }

                    if (hyphens != null && hyphens.indexOfKey(index + 1) > -1) { //Need to wrap text to next line
                        if (hyphens.get(index + 1)) { //Word is hyphenated
                            bRight = (x > 0 ? x + 2 : canvasWidth) + offsetX;
                            StringBuilder linkWordCopy = new StringBuilder(linkWord);
                            int indexCopy = index + 1;
                            while (DELIMITERS.indexOf(line.charAt(indexCopy)) == -1) {
                                linkWordCopy.append(line.charAt(indexCopy));
                                indexCopy++;
                            }
                            buttons.add(new WordButton(linkWordCopy.toString(), l, bIndex, bLeft, bRight, bTop, bBottom));
                            bLeft = offsetX;
                        }
                        y += size * options.fc_text_spacing;
                        bTop = y;
                        bBottom = Math.round(y + spacing);
                        x = 0;
                    }

                }
                if (!buttonStart) { //Button still needs to be added
                    bRight = (x > 0 ? x + 2 : canvasWidth) + offsetX;
                    buttons.add(new WordButton(linkWord.toString(), l, bIndex, bLeft, bRight, bTop, bBottom));
                }
                y += spacing;
            }
        }
    }

    //------------------------------------------------------------------------------------

    /**
     * Calculate sum of float array sub-interval
     *
     * @param array  Array to sum
     * @param start  Starting index (included)
     * @param finish Finishing index (excluded)
     * @return Sum of array values;
     */
    private float sumFloats(float[] array, int start, int finish) {
        float result = 0f;
        for (int i = start; i < finish; i++) {
            result += array[i];
        }
        return result;
    }

    /**
     * Calculate sum of integer array sub-interval
     *
     * @param array Array to sum
     * @return Sum of array values;
     */
    private int sumInts(int[] array) {
        int result = 0;
        for (int value : array) {
            result += value;
        }
        return result;
    }

    /**
     * Returns int sum of float values
     *
     * @param number Number of lines
     * @param size   Font size
     * @return Total height
     */
    private int getSizesSum(int number, float size) {
        int result = 0;
        for (int i = 0; i < number; i++) {
            result += size;
        }
        return result;
    }

    //------------------------------------------------------------------------------------

    /**
     * Utility method for drawing part of the chapter title
     *
     * @param canvas Canvas to draw to
     * @param line   Text to draw
     * @param x      X-axis position
     * @param y      Y-axis position
     */
    private void paintChapterTitleLine(Canvas canvas, String line, int x, int y) {
        float widths[] = new float[line.length()];
        paint.getTextWidths(line, widths);
        for (int i = 0; i < line.length(); i++) {
            char ch = line.charAt(i);
            if (ch == '[' || ch == ']' || ch == '{' || ch == '}') {
                continue;
            }
            if (dictionaryEnabled) { //Dictionary links enabled
                if (DELIMITERS.indexOf(ch) == -1) { //Part of the link
                    paint.setColor(options.fc_text_color_dict);
                    paint.setUnderlineText(true);
                } else { //Delimiter
                    paint.setColor(options.fc_text_color_cinnabar);
                    paint.setUnderlineText(false);
                }
            } else if (errorsEnabled) { //Error links enabled
                if (DELIMITERS.indexOf(ch) == -1) { //Part of the link
                    paint.setColor(options.fc_text_color_error);
                    paint.setUnderlineText(true);
                } else { //Delimiter
                    paint.setColor(options.fc_text_color_cinnabar);
                    paint.setUnderlineText(false);
                }
            }
            canvas.drawText(String.valueOf(ch), x, y, paint);
            x += widths[i];
        }
    }

    /**
     * Draws chapter title and text
     *
     * @param canvas Canvas to draw to
     * @param cn     Number of chapter to print
     * @param y      Starting y position
     */
    private void paintChapterText(Canvas canvas, int cn, int y) {
        float canvasWidth = this.getWidth();
        int canvasHeight = this.getHeight();
        String line;
        paint.setStrokeWidth(0);
        paint.setStyle(Paint.Style.FILL);

        paint.setTypeface(regularFont);

        line = book.getChapters().get(cn).getTitle();
        if (line != null && line.trim().length() > 0 && currentPositions.get(page).getLineNumber() == -1) { //Do not need to print
            paint.setTextSize(options.fc_text_size * COEF_SIZE_CHAPTER);

            float size = paint.getTextSize();
            float spacing = size * COEF_SPACING;
            float spaceWidth = paint.measureText(" ");

            //paint.setColor(options.fc_text_color_cinnabar);
            paint.setTypeface(boldFont);
            StringBuilder builder = new StringBuilder();
            int x = 0;
            String[] words = line.split(" "); //Words contained in title
            int startIndex = currentPositions.get(page).getCharIndex();
            int endIndex = (page < currentPositions.size() - 1 && currentPositions.get(page + 1).getLineNumber() == -1) ? currentPositions.get(page + 1)
                    .getCharIndex() : words.length;
            for (int index = startIndex; index < endIndex; index++) {
                String token = words[index];
                float width = paint.measureText(token);
                if (builder.length() > 0 && x + width > canvasWidth) { //Need to wrap text to next line
                    paintChapterTitleLine(canvas, builder.toString().trim(), Math.round((canvasWidth - x + spaceWidth) / 2), Math.round(y + size));
                    builder.setLength(0);
                    y += size * options.fc_text_spacing;
                    x = 0;
                }
                builder.append(token).append(' ');
                x += width + spaceWidth;
            }
            if (builder.length() > 0) { //Need to print rest of the title
                paintChapterTitleLine(canvas, builder.toString().trim(), Math.round((canvasWidth - x + spaceWidth) / 2), Math.round(y + size));
            }
            y += spacing;
            if (endIndex < words.length) {
                return;
            }
        }
        //---------------------------

        paint.setColor(options.fc_other_color_service);
        paint.setTypeface(regularFont); //TODO
        for (int i = 0; i < currentColumnCount - 1; i++) { //Draw separation lines
            float w = (i + 1) * this.getWidth() / currentColumnCount;
            canvas.drawLine(w, y - 1, w, canvasHeight - STATUS_HEIGHT, paint);
        }
        canvasWidth = this.getWidth() / currentColumnCount - INTERPAGE_PADDING * (currentColumnCount - 1);

        int storeY = y;
        for (int p = 0; p < currentColumnCount; p++) {
            float offsetX = (this.getWidth() / currentColumnCount) * (p % currentColumnCount) + (p % currentColumnCount > 0 ? INTERPAGE_PADDING : 0);
            y = storeY;

            List<String> text = bookText[cn];
            int startLine = (currentPositions.get(page + p).getLineNumber() > -1) ? currentPositions.get(page + p).getLineNumber() : 0;
            int endLine = (page + p < currentPositions.size() - 1 && currentPositions.get(page + p + 1).getLineNumber() > -1) ? currentPositions.get(
                    page + p + 1).getLineNumber() + 1 : text.size();
            for (int l = startLine; l < endLine; l++) {
                paint.setTextSize(options.fc_text_size);
                paint.setColor(options.fc_text_color());
                paint.setTypeface(regularFont); //TODO

                line = text.get(l);
                float x = 0f;
                SparseBooleanArray hyphens = currentHyphens.get(l);
                SparseArray<Float> spacings = currentSpacings.get(l);

                float size = paint.getTextSize();
                float spacing = size * COEF_SPACING;

                float spaceWidth = paint.measureText(" ");
                float[] charWidths = new float[line.length()];
                paint.getTextWidths(line, charWidths);

                paint.setUnderlineText(false);
                int startIndex = (currentPositions.get(page + p).getLineNumber() == l) ? currentPositions.get(page + p).getCharIndex() : 0;
                if (startIndex > 0 && startIndex < line.length()
                        && line.substring(0, startIndex).lastIndexOf('[') > line.substring(0, startIndex).lastIndexOf(']')) {
                    //paint.setColor(options.fc_text_color_cinnabar);
                    paint.setTypeface(boldFont);
                }
                if (startIndex > 0 && startIndex < line.length()
                        && line.substring(0, startIndex).lastIndexOf('{') > line.substring(0, startIndex).lastIndexOf('}')) {
                    //paint.setColor(options.fc_text_color_cinnabar);
                    paint.setTypeface(italicFont);
                }
                int endIndex = (page + p < currentPositions.size() - 1 && currentPositions.get(page + p + 1).getLineNumber() == l) ? currentPositions.get(
                        page + p + 1).getCharIndex() : line.length();
                for (int index = startIndex; index < endIndex; index++) { //For all characters
                    if (index >= line.length()) {
                        continue;
                    }
                    if (options.fillLine && spacings != null && spacings.indexOfKey(index) > -1) { //Word spacing needs to be adjusted
                        spaceWidth = spacings.get(index);
                    }
                    char ch = line.charAt(index);
                    switch (ch) {
                        case '[':
                            //paint.setColor(options.fc_text_color_cinnabar);
                            paint.setTypeface(boldFont);
                            continue;
                        case ']':
                            paint.setColor(options.fc_text_color());
                            paint.setTypeface(regularFont);
                            continue;
                        case '{':
//                            paint.setColor(options.fc_text_color_cinnabar);
                            paint.setTypeface(italicFont);
                            continue;
                        case '}':
                            paint.setColor(options.fc_text_color());
                            paint.setTypeface(regularFont);
                            continue;
                    }

                    if (dictionaryEnabled) { //Dictionary links enabled
                        if (DELIMITERS.indexOf(ch) == -1) { //Part of the link
                            paint.setColor(options.fc_text_color_dict);
                            paint.setUnderlineText(true);
                        } else { //Delimiter
                            paint.setColor(options.fc_text_color());
                            paint.setUnderlineText(false);
                        }
                    } else if (errorsEnabled) { //Error links enabled
                        if (DELIMITERS.indexOf(ch) == -1) { //Part of the link
                            paint.setColor(options.fc_text_color_error);
                            paint.setUnderlineText(true);
                        } else { //Delimiter
                            paint.setColor(options.fc_text_color());
                            paint.setUnderlineText(false);
                        }
                    }

                    canvas.drawText(String.valueOf(ch), x + offsetX, y + size, paint);
                    if (options.fillLine && ch == ' ') { //Space character
                        x += spaceWidth;
                    } else { //Other character
                        x += charWidths[index];
                    }

                    if (hyphens != null && hyphens.indexOfKey(index + 1) > -1) { //Need to wrap text to next line
                        if (hyphens.get(index + 1)) { //Need to draw hyphenation mark
                            canvas.drawText(HYPHENATOR, x + offsetX, y + size, paint);
                        }
                        y += size * options.fc_text_spacing;
                        x = 0f;
                    }
                }

                y += spacing;
            }
            paint.setUnderlineText(false);

        }

        //--- Pressed buttons ---
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(options.fc_text_color_selected);
        paint.setStrokeWidth(2);
        if (wordButtonPressed > -1 && wordButtonPressed < wordButtons.size()) { //Paint pressed button
            canvas.drawRoundRect(wordButtons.get(wordButtonPressed).getRectangle(), 2f, 2f, paint);
        }
        paint.setStrokeWidth(0);
        //---
    }

    //------------------------------------------------------------------------------------

    /**
     * Draws book title (on separate page)
     *
     * @param canvas Canvas to draw to
     * @param book   Book metadata
     */
    private void paintBookTitle(Canvas canvas, Book book) {
        int canvasHeight = this.getHeight() - STATUS_HEIGHT;
        int canvasWidth = this.getWidth();

        paint.setTextSize(options.fc_text_size * COEF_SIZE_BOOK_TITLE);
        paint.setTypeface(headerFont);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(options.fc_text_color_cinnabar);

        float spacing = paint.getFontSpacing();
        float spaceWidth = paint.measureText(" ");

        int lY = 0;
        int lX = 0;

        StringBuilder builder = new StringBuilder();
        List<String> lines = new ArrayList<String>();
        int x = lX;

        for (String token : book.getTitle().toUpperCase().split(" ")) { //Calculate number of lines
            float width = paint.measureText(token);
            if (builder.length() > 0 && x + width > canvasWidth) { //Need to wrap text to next title
                lines.add(builder.toString().trim());
                builder.setLength(0);
                lY += spacing;
                x = lX;
            }
            builder.append(token).append(' ');
            x += width + spaceWidth;
        }

        if (builder.length() > 0) { //Need to print rest of the title
            lines.add(builder.toString().trim());
        }
        lY += spacing;
        int y = (canvasHeight - lY - (int) spacing) * 2 / 5;
        for (String s : lines) { //Paint lines
            canvas.drawText(s, Math.round((canvasWidth - paint.measureText(s)) / 2), y + spacing, paint);
            y += spacing;
        }

        //Author name
        paint.setTextSize(options.fc_text_size * COEF_SIZE_BOOK_AUTHOR);
        paint.setTypeface(regularFont);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(options.fc_text_color());
        canvas.drawText(book.getAuthor(), Math.round((canvasWidth - paint.measureText(book.getAuthor())) / 2), y + spacing, paint);
    }

    //------------------------------------------------------------------------------------

    /**
     * Draws status bar
     *
     * @param canvas Canvas to draw to
     */
    private void paintStatusBar(Canvas canvas) {
        int canvasHeight = this.getHeight();
        int canvasWidth = this.getWidth();

        paint.setColor(options.fc_other_color_service);
        paint.setTypeface(Typeface.DEFAULT);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize(STATUS_SIZE);
        String right = String.format("%02d:%02d", time / 60, time % 60);
        float rw = paint.measureText(right);
        canvas.drawText(right, canvasWidth - rw, canvasHeight, paint);
        if (globalSize == 0 || (bookCalculator != null && !bookCalculator.isFinished())) { //Waiting...
            canvas.drawText(waitWord, 0, canvasHeight, paint);
        } else { //Draw page indicator
            String left = String.format("%d/%d", (globalPage + currentColumnCount) / currentColumnCount, globalSize / currentColumnCount);
            float lw = paint.measureText(left);
            canvas.drawText(left, 0, canvasHeight, paint);
            paint.setAntiAlias(false);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawRect(lw + STATUS_PADDING, canvasHeight - STATUS_SCROLL - 1, canvasWidth - rw - STATUS_PADDING, canvasHeight - 1, paint);
            paint.setStyle(Paint.Style.FILL);
            float multiplier = canvasWidth - rw - lw - STATUS_PADDING * 2;
            float length = (globalSize == currentColumnCount) ? multiplier : (globalPage * multiplier / (globalSize - currentColumnCount));
            canvas.drawRect(lw + STATUS_PADDING, canvasHeight - STATUS_SCROLL - 1, lw + STATUS_PADDING + length, canvasHeight - 1, paint);
            paint.setStyle(Paint.Style.STROKE);
            for (int i = 1; i < globalIndicies.length - 1; ++i) {
                float tick = (globalSize == currentColumnCount) ? multiplier : ((globalIndicies[i] - currentColumnCount) * multiplier / (globalSize - currentColumnCount));
                if (tick > canvasWidth - rw - lw - 2 * STATUS_PADDING - 0.5f) { //Last tick
                    continue;
                }
                paint.setColor(globalIndicies[i] - currentColumnCount < globalPage ? options.fc_back_color() : options.fc_other_color_service);
                canvas.drawLine(lw + STATUS_PADDING + tick, canvasHeight - STATUS_SCROLL, lw + STATUS_PADDING + tick, canvasHeight - 1, paint);
            }
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.FILL);
        }
    }

    //------------------------------------------------------------------------------------

    /**
     * Draws book contents
     *
     * @param canvas    Canvas to draw to
     * @param drawTitle true if book title needs to be drawn, false otherwise
     */
    private void paintContents(Canvas canvas, boolean drawTitle) {
        int canvasWidth = this.getWidth();
        int canvasHeight = this.getHeight();
        String line;
        int y = 0;

        if (book == null || canvasWidth == 0) {
            return;
        }

        if (drawTitle) { //Need to draw book title
            conPaint.setTextSize(options.fc_text_size);
            conPaint.setTypeface(Typeface.DEFAULT);
            line = book.getTitle();

            float spacing = conPaint.getFontSpacing() * COEF_SPACING_CONTENTS_HEADER;
            float size = conPaint.getTextSize() * COEF_SIZE_CONTENTS_HEADER;
            float spaceWidth = conPaint.measureText(" ");

            conPaint.setStyle(Paint.Style.FILL);
            conPaint.setTypeface(boldFont);
            conPaint.setColor(conButtonPressed == -1 ? options.fc_text_color_selected : options.fc_text_color_cinnabar);
            StringBuilder builder = new StringBuilder();
            int x = 0;
            String[] words = line.split(" "); //Words contained in title
            int startIndex = conPage > 0 && conPage < contentPositions.size() ? contentPositions.get(conPage).getCharIndex() : 0;
            int endIndex = (conPage < contentPositions.size() - 1 && contentPositions.get(conPage + 1).getLineNumber() == -1) ? contentPositions.get(
                    conPage + 1).getCharIndex() : words.length;
            for (int index = startIndex; index < endIndex; index++) {
                String token = words[index];
                float width = conPaint.measureText(token);

                if (builder.length() > 0 && x + width > canvasWidth) { //Need to wrap text to next line
                    canvas.drawText(builder.toString().trim(), Math.round((canvasWidth - x + spaceWidth) / 2), y + size + options.fc_text_size / 4, conPaint);
                    builder.setLength(0);
                    y += size;
                    x = 0;
                }
                builder.append(token).append(' ');
                x += width + spaceWidth;
            }
            if (builder.length() > 0) { //Need to print rest of the title
                canvas.drawText(builder.toString(), Math.round((canvasWidth - x + spaceWidth) / 2), y + size + options.fc_text_size / 4, conPaint);
            }
//            y += spacing;
            y = conHeaderOffset;
        }

        conPaint.setColor(options.fc_other_color_service);
        canvas.drawLine(0, y - 1, canvasWidth, y - 1, conPaint);

        canvasWidth = this.getWidth() / currentColumnCount - INTERPAGE_PADDING * (currentColumnCount - 1);

        //------------------------
        conPaint.setTextSize(options.fc_text_size * COEF_SIZE_CONTENTS_ITEM);
        float size = conPaint.getTextSize();
        int interspace = Math.round(size * COEF_INTERSPACE_CONTENTS);
        y += interspace;

        int storeY = y;
        for (int p = 0; p < currentColumnCount; p++) {
            int offsetX = (this.getWidth() / currentColumnCount) * (p % currentColumnCount) + (p % currentColumnCount > 0 ? INTERPAGE_PADDING : 0);
            y = storeY;

            int startLine = (contentPositions.get(conPage + p).getLineNumber() > -1) ? contentPositions.get(conPage + p).getLineNumber() : 0;
            int endLine = (conPage + p < contentPositions.size() - 1 && contentPositions.get(conPage + p + 1).getLineNumber() > -1) ? contentPositions.get(
                    conPage + p + 1).getLineNumber() + 1 : book.getChapters().size();
            for (int l = startLine; l < endLine; l++) {
                if (l >= book.getChapters().size()) { //Out of bounds (probably last page)
                    continue;
                }
                conPaint.setStyle(Paint.Style.FILL);
                conPaint.setColor(l == conButtonPressed ? options.fc_text_color_selected : (l == chapter ? options.fc_text_color_cinnabar : options
                        .fc_text_color()));
                conPaint.setTypeface(l == chapter ? boldFont : regularFont);
                line = "  " + book.getChapters().get(l).getTitle();
                int x = 0;
                float[] charWidths = new float[line.length()];
                conPaint.getTextWidths(line, charWidths);

                boolean wordStart = true;
                int startIndex = (contentPositions.get(conPage + p).getLineNumber() == l) ? contentPositions.get(conPage + p).getCharIndex() : 0;
                int endIndex = (conPage + p < contentPositions.size() - 1 && contentPositions.get(conPage + p + 1).getLineNumber() == l) ? contentPositions
                        .get(conPage + p + 1).getCharIndex() : line.length();
                int cin = 1;
                if (line.charAt(2) == '-') { //Title is empty
                    cin = 0;
                } else {
                    for (int i = 3; i < line.length(); i++) {
                        if ("0123456789".indexOf(line.charAt(i)) > -1) { //Character is a digit
                            cin++;
                        } else { //Character is a letter
                            break;
                        }
                    }
                }
                for (int index = startIndex; index < endIndex; index++) { //For all characters
                    if (l != chapter && l != conButtonPressed) { //First letter selected
                        if (index == 0) { //Start of line
                            conPaint.setColor(options.fc_text_color_cinnabar);
                        } else if (index == cin + 2) { //After first letter
                            conPaint.setColor(options.fc_text_color());
                        }
                    }
                    String c = String.valueOf(line.charAt(index));
                    float width = charWidths[index];

                    if (wordStart) { //Start of the word
                        int f = line.indexOf(' ', index);
                        if (x + sumFloats(charWidths, index, f > -1 ? f : line.length()) > canvasWidth) { //Need to wrap text to next line
                            y += size;
                            x = 0;
                        }
                        wordStart = false;
                    }

                    canvas.drawText(c, x + offsetX, y + size, conPaint);
                    x += width;
                    if (line.charAt(index) == ' ') { //Need to start new word
                        wordStart = true;
                    }
                }
                y += size;
                y += interspace;
            }
        }

        //--- Separation lines (multipage) ---
        paint.setColor(options.fc_other_color_service);
        for (int i = 0; i < currentColumnCount - 1; i++) { //Draw separation lines
            int w = (i + 1) * this.getWidth() / currentColumnCount;
            canvas.drawLine(w, conHeaderOffset, w, this.getHeight() - conFooterOffset, paint);
        }
        //---

        //--- Pressed buttons ---
        conPaint.setStyle(Paint.Style.STROKE);
        conPaint.setColor(options.fc_text_color_selected);
        conPaint.setStrokeWidth(2);
        if (conButtonPressed == -1) { //Paint pressed button
            canvas.drawRoundRect(new RectF(1, 1, canvasWidth - 1, conHeaderOffset - 1), 2f, 2f, conPaint);
        }
        if (conButtonPressed > -1 && conButtonPressed < contentButtons.size()) { //Paint pressed button
            canvas.drawRoundRect(contentButtons.get(conButtonPressed).getRectangle(), 2f, 2f, conPaint);
        }
        conPaint.setStrokeWidth(0);
        //---

        conPaint.setStyle(Paint.Style.FILL);
        if (conTotalPages > currentColumnCount) { //Need to draw navigation items
            canvasWidth = this.getWidth();
            conPaint.setColor(options.fc_other_color_service);
            conPaint.setTextSize(options.fc_text_size * COEF_SIZE_CONTENTS_ITEM);
            conPaint.setTypeface(regularFont);
            String text = String.format("%d/%d", (conPage + currentColumnCount) / currentColumnCount, conTotalPages / currentColumnCount);
            canvas.drawText(text, Math.round((canvasWidth - conPaint.measureText(text)) / 2), canvasHeight - (conFooterOffset - conPaint.getTextSize() + 1)
                    / 2, conPaint);
            canvas.drawLine(0, canvasHeight - conFooterOffset, this.getWidth(), canvasHeight - conFooterOffset, conPaint);
            float bw = conPaint.measureText(backWord);
            float fw = conPaint.measureText(forwWord);
            conPaint.setColor(conButtonPressed == -3 ? options.fc_text_color_selected : options.fc_text_color());
            canvas.drawText(backWord, STATUS_PADDING, canvasHeight - (conFooterOffset - conPaint.getTextSize() + 1) / 2, conPaint);
            conPaint.setColor(conButtonPressed == -4 ? options.fc_text_color_selected : options.fc_text_color());
            canvas.drawText(forwWord, canvasWidth - fw - STATUS_PADDING, canvasHeight - (conFooterOffset - conPaint.getTextSize() + 1) / 2, conPaint);
            conPaint.setStyle(Paint.Style.STROKE);
            conPaint.setColor(options.fc_text_color_selected);
            conPaint.setStrokeWidth(2);
            if (conButtonPressed == -3) { //Paint pressed back button
                canvas.drawRoundRect(new RectF(1, canvasHeight - conFooterOffset + 1, bw + STATUS_PADDING * 2 - 1, canvasHeight - 1), 2f, 2f, conPaint);
            }
            if (conButtonPressed == -4) { //Paint pressed forward button
                canvas.drawRoundRect(new RectF(canvasWidth - fw - STATUS_PADDING * 2 + 1, canvasHeight - conFooterOffset + 1, canvasWidth - 1,
                        canvasHeight - 1), 2f, 2f, conPaint);
            }
            conPaint.setStrokeWidth(0);
        }
    }

    //------------------------------------------------------------------------------------

    /**
     * Draws brightness bar
     *
     * @param canvas Canvas to draw to
     */
    private void paintBrightnessBar(Canvas canvas) {
        int centerVert = this.getHeight() / 2;
        paint.setAntiAlias(false);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(options.fc_back_color());
        canvas.drawRect(BRIGHT_FRAME_MARGIN - 1, centerVert - BRIGHT_FRAME_HEIGHT / 2 - 1, BRIGHT_FRAME_MARGIN + BRIGHT_FRAME_WIDTH + 2, centerVert + BRIGHT_FRAME_HEIGHT / 2 + 2, paint);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(options.fc_other_color_service);
        canvas.drawRect(BRIGHT_FRAME_MARGIN, centerVert - BRIGHT_FRAME_HEIGHT / 2, BRIGHT_FRAME_MARGIN + BRIGHT_FRAME_WIDTH, centerVert + BRIGHT_FRAME_HEIGHT / 2, paint);
        paint.setColor(options.fc_other_color_service);
        canvas.drawRect(BRIGHT_FRAME_MARGIN + BRIGHT_FRAME_PADDING, centerVert - BRIGHT_FRAME_HEIGHT / 2 + BRIGHT_FRAME_PADDING, BRIGHT_FRAME_MARGIN + BRIGHT_FRAME_PADDING + BRIGHT_FRAME_THICK, centerVert + BRIGHT_FRAME_HEIGHT / 2 - BRIGHT_FRAME_PADDING, paint);
        paint.setStyle(Paint.Style.FILL);
        int barTop = centerVert + BRIGHT_FRAME_HEIGHT / 2 - BRIGHT_FRAME_PADDING - Math.round(brightness * (BRIGHT_FRAME_HEIGHT - 2 * BRIGHT_FRAME_PADDING));
        canvas.drawRect(BRIGHT_FRAME_MARGIN + BRIGHT_FRAME_PADDING, barTop, BRIGHT_FRAME_MARGIN + BRIGHT_FRAME_PADDING + BRIGHT_FRAME_THICK, centerVert + BRIGHT_FRAME_HEIGHT / 2 - BRIGHT_FRAME_PADDING, paint);
        paint.setAntiAlias(true);
    }

    //------------------------------------------------------------------------------------

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (currentPositions == null || page < 0 || page > currentPositions.size() - currentColumnCount) {
            return;
        }

        if (contentsShowing) { //Book contents
            //--- Book contents ---
            paintContents(canvas, true);
            if (animation != 0 && bitmapStorage != null && options.animation) {
                int offset = -this.getWidth() / ANIM_LENGTH * animation;
                canvas.drawBitmap(bitmapStorage, offset, conHeaderOffset, conPaint);
                conPaint.setColor(options.fc_other_color_service);
                offset = offset >= 0 ? offset : offset + getWidth();
                canvas.drawLine(offset, conHeaderOffset, offset, this.getHeight() - conFooterOffset, conPaint);
                //canvas.drawLine(animation > 0 ? 0 : this.getWidth(), conTitleOffset - 1, offset, conTitleOffset - 1, conPaint);
            }
        } else { //Regular text
            if (scaleBegan) {
                canvas.save();
                canvas.scale(scaleFactor, scaleFactor, canvas.getWidth() / 2, canvas.getHeight() / 2);
            }
            paintStatusBar(canvas);
            if (chapter < 0) { //Book title
                paintBookTitle(canvas, book);
            } else { //Chapter
                paintChapterText(canvas, chapter, 0);
            }
            if (animation != 0 && bitmapStorage != null && options.animation) {
                int offset = -this.getWidth() / ANIM_LENGTH * animation;
                canvas.drawBitmap(bitmapStorage, offset, 0f, paint);
                paint.setColor(options.fc_other_color_service);
                offset = offset >= 0 ? offset : offset + getWidth();
                canvas.drawLine(offset, 0f, offset, this.getHeight() - STATUS_HEIGHT, paint);
            }
            if (scaleBegan) {
                canvas.restore();
            }
        }

        if (brightnessTimeout != 0) { //Brightness frame is visible
            paintBrightnessBar(canvas);
        }

    }

    //------------------------------------------------------------------------------------

    /**
     * Draws current canvas contents to bitmap object
     */
    private void storeCurrentBitmap() {
        if (book == null) { //Precaution
            return;
        }
        int height = contentsShowing ? this.getHeight() - conHeaderOffset - conFooterOffset : this.getHeight() - STATUS_HEIGHT;
        bitmapStorage = Bitmap.createBitmap(this.getWidth(), height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmapStorage);
        c.drawColor(options.fc_back_color());

        if (contentsShowing) { //Book contents
            paintContents(c, false);
        } else { //Regular text
            if (chapter < 0) { //Book title
                paintBookTitle(c, book);
            } else { //Chapter
                paintChapterText(c, chapter, 0);
            }
        }
    }

    //------------------------------------------------------------------------------------

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        this.postDelayed(new Runnable() {
            @Override
            public void run() {
                requestPositionRelocation(null);
            }
        }, 500);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.postDelayed(new Runnable() {
            @Override
            public void run() {
                requestPositionRelocation(null);
            }
        }, 500);
    }

    /**
     * Requests component to be re-validated in order to find target position
     *
     * @param position Position to search or null for current position
     */
    public void requestPositionRelocation(Position position) {
        if (options == null || book == null) { //No book currently displayed
            return;
        }
        currentColumnCount = (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) ? options.colLand : options.colPort;
        animation = 0;
        touch = 0;
        stopTouchThread();
        stopAnimationThread();
        brightnessTimeout = 0;
        scaleBegan = false;
        if (position == null) {
            if (currentPositions == null || page < 0 || page > currentPositions.size() - 1) { //Vital variables undefined
                position = new Position(-1);
            } else { //Able to define current position
                position = new Position(currentPositions.get(page));
            }
        }
        chapter = position.getChapterNumber();
        CalculationResult result = calculate(book, bookText, chapter, new Position(position), alwaysFalseCancelListener);
        currentPositions = result.positions;
        currentHyphens = result.hyphens;
        currentSpacings = result.spacings;
        maxPages = result.totalPages;
        page = result.targetPage;
        if (contentsShowing) { //Book contents shown
            switchContents(true);
        }
        switchDictionary(false);
        switchErrors(false);
        BRIGHT_FRAME_HEIGHT = Math.min(this.getWidth(), this.getHeight()) - 2 * BRIGHT_FRAME_MARGIN;
        BRIGHT_FRAME_WIDTH = 2 * BRIGHT_FRAME_PADDING + BRIGHT_FRAME_THICK;
        BRIGHT_FLING_STEP = Math.round((float) (BRIGHT_FRAME_HEIGHT - 2 * BRIGHT_FRAME_PADDING) * BRIGHT_VALUE_STEP);
        //--- Book calculator ---
        if (bookCalculator != null) {
            bookCalculator.cancel(true);
        }
        bookCalculator = new BookCalculatorTask(this);
        bookCalculator.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        //---
        this.invalidate();
    }

    //------------------------------------------------------------------------------------
    /**
     * Listener for all touch events
     */
    private OnTouchListener onTouchListener = new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            scaleGestureDetector.onTouchEvent(event);
            if (scaleBegan) { //Already processing scale gesture
                return true;
            }
            float xx = event.getX();
            float yy = event.getY();

            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN: //---------------------------------------------------
                    if (contentsShowing) { //Book contents shown
                        if (event.getY() < conHeaderOffset) { //Title pressed
                            conButtonPressed = -1;
                            invalidate();
                        } else if (event.getY() >= getHeight() - conFooterOffset) {
                            conButtonPressed = event.getX() <= getWidth() / 2 ? -3 : -4;
                            invalidate();
                        } else {
                            for (int i = 0; i < contentButtons.size(); i++) {
                                if (contentButtons.get(i).check(conPage, (int) event.getX(), (int) event.getY())) { //Button pressed
                                    conButtonPressed = i;
                                    invalidate();
                                    break;
                                }
                            }
                        }
                        return true;
                    }

                    if (dictionaryEnabled || errorsEnabled) { //Word links visible
                        for (int i = 0; i < wordButtons.size(); i++) {
                            if (wordButtons.get(i).check(event.getX(), event.getY())) { //Button pressed
                                wordButtonPressed = i;
                                invalidate();
                                break;
                            }
                        }
                        return true;
                    }

                    touchX = (int) event.getX();
                    touchY = (int) event.getY();
                    touchZone = getZoneForCoords(touchX, touchY);
                    touch = TOUCH_ACTION;
                    if (quickActionListener != null) {
                        if (quickActionListener.checkNextPageLongAction(touchZone)) {
                            storeCurrentBitmap();
                            touch = 1;
                        }
                        if (quickActionListener.checkPreviousPageLongAction(touchZone)) {
                            storeCurrentBitmap();
                            touch = -1;
                        }
                    }
                    startTouchThread();
                    break;

                case MotionEvent.ACTION_UP: //---------------------------------------------------
                    stopTouchThread();
                    if (brightnessTimeout < 0) {
                        changeBrightness(brightness, true);
                    }

                    if (contentsShowing) { //Book contents shown
                        if (event.getY() < conHeaderOffset) { //Title is still pressed
                            calculateFirstPage();
                            switchContents(false);
                        } else if (event.getY() >= getHeight() - conFooterOffset) {
                            if (conButtonPressed == -3 && event.getX() <= getWidth() / 2) { //Back button is still pressed
                                goToContentsPreviousPage();
                            } else if (conButtonPressed == -4 && event.getX() > getWidth() / 2) { //Forward button is still pressed
                                goToContentsNextPage();
                            }
                        } else if (conButtonPressed > -1 && conButtonPressed < contentButtons.size()
                                && contentButtons.get(conButtonPressed).check(conPage, (int) event.getX(), (int) event.getY())) { //Button is still pressed
                            calculateSpecificChapter(conButtonPressed);
                            switchContents(false);
                        }
                        conButtonPressed = -2;
                        invalidate();
                        return true;
                    }

                    if (dictionaryEnabled || errorsEnabled) { //Word links visible
                        if (wordButtonPressed > -1) {
                            if (wordButtonPressed < wordButtons.size() && wordButtons.get(wordButtonPressed).check(event.getX(), event.getY())) { //Button is still pressed
                                WordButton button = wordButtons.get(wordButtonPressed);
                                if (dictionaryEnabled) { //Dictionary links visible
                                    if (dictionaryListener != null) {
                                        dictionaryListener.onLinkClicked(button.getWord(), chapter, button.getLine(), button.getIndex());
                                    }
                                    switchDictionary(false);
                                } else if (errorsEnabled) { //Errors links visible
                                    if (errorsListener != null) {
                                        errorsListener.onLinkClicked(button.getWord(), chapter, button.getLine(), button.getIndex());
                                    }
                                    switchErrors(false);
                                }
                            }
                            wordButtonPressed = -1;
                            invalidate();
                        }
                        return true;
                    }

                    ReaderZone zone = getZoneForCoords(xx, yy);

                    if (Math.abs(yy - touchY) <= FLING_VERT_THRESHOLD && Math.abs(xx - touchX) >= FLING_HORZ_MINIMAL) {
                        //Fling functionality
                        touch = xx > touchX ? -1 : 1;
                        touchZone = zone;
                    }

                    if (zone == touchZone) { //The same zone is pressed (finger was not moved)
                        if (touch == TOUCH_ACTION) { //Custom action
                            if (quickActionListener != null) {
                                quickActionListener.onQuickActionTriggered(zone, false);
                            }
                        } else if (touch == 1) { //Next page
                            goToNextPage(1);
                        } else if (touch == -1) { //Previous page
                            goToPreviousPage(1);
                        } else if (touch != TOUCH_ACTION + 1 && touch != 0) {
                            animation = touch / Math.abs(touch);
                            startAnimationThread();
                        }

                    }
                    touch = 0;
                    break;

                case MotionEvent.ACTION_MOVE: //---------------------------------------------------
                    if (contentsShowing) { //Book contents shown
                        if ((conButtonPressed == -1 && event.getY() > conHeaderOffset)
                                || (conButtonPressed == -3 && (event.getY() < getHeight() - conFooterOffset || event.getX() > getWidth() / 2))
                                || (conButtonPressed == -4 && (event.getY() < getHeight() - conFooterOffset || event.getX() < getWidth() / 2))
                                || (conButtonPressed > -1 && conButtonPressed < contentButtons.size() && !contentButtons.get(conButtonPressed).check(conPage,
                                (int) event.getX(), (int) event.getY()))) {//Button is not pressed anymore
                            conButtonPressed = -2;
                            invalidate();
                        }
                    }
                    if (dictionaryEnabled || errorsEnabled) { //Word links visible
                        if (wordButtonPressed > -1 && wordButtonPressed < wordButtons.size()
                                && !wordButtons.get(wordButtonPressed).check(event.getX(), event.getY())) { //Button is not pressed anymore
                            wordButtonPressed = -1;
                            invalidate();
                        }
                    }

                    if (brightnessTimeout < 0 || (xx < BRIGHT_FRAME_MARGIN + BRIGHT_FRAME_WIDTH && Math.abs(xx - touchX) <= BRIGHT_FLING_THRESH && Math.abs(yy - touchY) >= BRIGHT_FLING_STEP)) { //Adjust brightness
                        stopTouchThread();
                        changeBrightness(Math.max(BRIGHT_VALUE_MIN, Math.min(BRIGHT_VALUE_MAX, brightness + (touchY - yy) / BRIGHT_FLING_STEP * BRIGHT_VALUE_STEP)), false);
                        touchX = xx;
                        touchY = yy;
                        touchZone = null;
                        brightnessTimeout = -1;
                    }
                    break;

                case MotionEvent.ACTION_CANCEL: //---------------------------------------------------
                    stopTouchThread();
                    if (brightnessTimeout < 0) {
                        changeBrightness(brightness, true);
                    }
                    break;
            }
            return true;
        }
    };

    //------------------------------------------------------------------------------------

    private ScaleGestureDetector.OnScaleGestureListener onScaleGestureListener = new ScaleGestureDetector.OnScaleGestureListener() {

        private float startingSpan = 1f;
        private final float SCALE_REDUCTION_IN = 0.3f;
        private final float SCALE_REDUCTION_OUT = 0.6f;


        private int refineNewFontSize(float scaleFactor) {
            int result = Math.round(options.fc_text_size * scaleFactor);
            if (result < FONTSIZE_MIN) {
                result = FONTSIZE_MIN;
            }
            if (result > FONTSIZE_MAX) {
                result = FONTSIZE_MAX;
            }
            return result;
        }

        private float adjustScaleFactor(float scaleFactor) {
            int result = Math.round(options.fc_text_size * scaleFactor);
            if (result < FONTSIZE_MIN) {
                return FONTSIZE_MIN / options.fc_text_size;
            }
            if (result > FONTSIZE_MAX) {
                return FONTSIZE_MAX / options.fc_text_size;
            }
            return scaleFactor;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            scaleBegan = false;
            float scaleFactor = detector.getCurrentSpan() / startingSpan;
            scaleFactor = 1f + (scaleFactor - 1f) * (scaleFactor > 1f ? SCALE_REDUCTION_IN : SCALE_REDUCTION_OUT);
            options.fc_text_size = refineNewFontSize(scaleFactor);
            if (textSizeListener != null) {
                textSizeListener.onTextSizeChanged(options.fc_text_size);
            }
            requestPositionRelocation(null);
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            if (contentsShowing || dictionaryEnabled || errorsEnabled) {
                return false;
            }
            scaleBegan = true;
            scaleFactor = 1f;
            animation = 0;
            touch = 0;
            conButtonPressed = -2;
            wordButtonPressed = -1;
            stopTouchThread();
            stopAnimationThread();
            brightnessTimeout = 0;
            startingSpan = detector.getCurrentSpan();
            return true;
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            if (scaleBegan) {
                scaleFactor = detector.getCurrentSpan() / startingSpan;
                scaleFactor = 1f + (scaleFactor - 1f) * (scaleFactor > 1f ? SCALE_REDUCTION_IN : SCALE_REDUCTION_OUT);
                scaleFactor = adjustScaleFactor(scaleFactor);
                invalidate();
            }
            return true;
        }
    };

    //------------------------------------------------------------------------------------

    /**
     * Starts new LongTouchThread stopping any thread created earlier
     */
    private void startTouchThread() {
        stopTouchThread();
        ltThread = new LongTouchThread();
        ltThread.start();
    }

    /**
     * Stops current LongTouchThread if any running
     */
    private void stopTouchThread() {
        if (ltThread != null && ltThread.isRunning()) {
            ltThread.shutdown();
        }
    }

    /**
     * Starts new AnimationThread stopping any thread created earlier
     */
    private void startAnimationThread() {
        stopAnimationThread();
        anThread = new AnimationThread();
        anThread.start();
    }

    /**
     * Stops current AnimationThread if any running
     */
    private void stopAnimationThread() {
        if (anThread != null && anThread.isRunning()) {
            anThread.shutdown();
        }
    }

    //------------------------------------------------------------------------------------

    /**
     * Thread used to perform long touches
     */
    private class LongTouchThread extends Thread {
        private boolean running = true;

        boolean isRunning() {
            return running;
        }

        @Override
        public void run() {
            int delay = 500;
            while (running && touch != 0) {
                try {
                    Thread.sleep(delay);
                } catch (Exception ex) {
                    //Should normally never happen
                }
                if (!running || touch == 0) {
                    break;
                }
                if (touch == TOUCH_ACTION) { //Menu
                    handler.sendMessage(handler.obtainMessage(TOUCH_ACTION));
                    touch++;
                    break;
                } else if (touch < 0) { //Previous
                    handler.sendMessage(handler.obtainMessage(-1));
                    touch--;
                    delay = (touch > -6 ? 400 : (touch > -12 ? 300 : (touch > -20 ? 100 : 50)));
                } else if (touch > 0) { //Next
                    handler.sendMessage(handler.obtainMessage(1));
                    touch++;
                    delay = (touch < 6 ? 400 : (touch < 12 ? 300 : (touch < 20 ? 100 : 50)));
                }
            }
        }

        private void shutdown() {
            running = false;
        }
    }

    //------------------------------------------------------------------------------------

    /**
     * Thread used to perform animations
     */
    private class AnimationThread extends Thread {
        private boolean running = true;

        boolean isRunning() {
            return running;
        }

        @Override
        public void run() {
            if (!options.animation) {
                animation = 0;
            }
            while (running && animation != 0) {
                try {
                    Thread.sleep(50);
                } catch (Exception ex) {
                    //Should normally never happen
                }
                if (!running || animation == 0) {
                    break;
                }
                if (animation > 0) {
                    animation++;
                } else if (animation < 0) {
                    animation--;
                }
                if (Math.abs(animation) > ANIM_LENGTH) {
                    animation = 0;
                }
                handler.sendMessage(handler.obtainMessage(0));
            }
        }

        private void shutdown() {
            running = false;
        }
    }

    //------------------------------------------------------------------------------------

    /**
     * Thread used to count time during which the brightness frame is displayed
     */
    private class BrightnessThread extends Thread {
        private boolean running = true;

        boolean isRunning() {
            return running;
        }

        @Override
        public void run() {
            while (running && brightnessTimeout > 0) {
                try {
                    Thread.sleep(100);
                } catch (Exception ex) {
                    //Should normally never happen
                }
                if (!running || brightnessTimeout == 0) {
                    brightnessTimeout = 0;
                    break;
                }
                brightnessTimeout--;
            }
            running = false;
            handler.sendMessage(handler.obtainMessage(0));
        }
    }

    //------------------------------------------------------------------------------------

    /**
     * Handler for processing requests from background thread
     */
    private static class UniversalHandler extends Handler {
        private final WeakReference<BookReader> reader;

        UniversalHandler(BookReader reader) {
            this.reader = new WeakReference<BookReader>(reader);
        }

        @Override
        public void handleMessage(Message msg) {
            BookReader r = reader.get();
            switch (msg.what) {
                case 0: //Invalidate
                    r.invalidate();
                    break;
                case -1: //Go to previous page 10 times
                    for (int i = 0; i < 10; i++) {
                        r.calculatePreviousPage();
                    }
                    break;
                case 1: //Go to next page 10 times
                    for (int i = 0; i < 10; i++) {
                        r.calculateNextPage();
                    }
                    break;
                case TOUCH_ACTION: //Execute long action
                    if (r.quickActionListener != null) {
                        r.quickActionListener.onQuickActionTriggered(r.touchZone, true);
                    }
                    break;
            }
        }
    }

    //------------------------------------------------------------------------------------

    /**
     * Listener to quick actions triggered by users
     */
    public interface OnQuickActionsListener {

        /**
         * Triggered when user presses part of the screen
         *
         * @param zone   Part user presses
         * @param isLong true if long action triggered, false for short action
         */
        void onQuickActionTriggered(ReaderZone zone, boolean isLong);

        /**
         * Determine whether long action can be performed in specific zone
         *
         * @param zone Zone to check
         * @return true if long next page action is set to this zone
         */
        boolean checkNextPageLongAction(ReaderZone zone);

        /**
         * Determine whether long action can be performed in specific zone
         *
         * @param zone Zone to check
         * @return true if long previous page action is set to this zone
         */
        boolean checkPreviousPageLongAction(ReaderZone zone);
    }

    //------------------------------------------------------------------------------------

    /**
     * Listener to quick actions triggered by users
     */
    public interface OnBrightnessListener {

        /**
         * Triggered when brightness was changed
         *
         * @param newValue New brightness value (percentage)
         */
        void onBrightnessChanged(float newValue);
    }

    //------------------------------------------------------------------------------------

    /**
     * Listener for various link buttons clicks
     */
    public interface OnWordLinkClickedListener {
        /**
         * Called when link clicked
         *
         * @param word    Linked word
         * @param chapter Chapter number
         * @param line    Line number
         * @param index   Character index
         */
        void onLinkClicked(String word, int chapter, int line, int index);
    }

    //------------------------------------------------------------------------------------

    /**
     * Listener for text size changes
     */
    public interface OnTextSizeChangedListener {
        /**
         * Called when text size is changed (pinch gesture)
         *
         * @param newSize New size of the text
         */
        void onTextSizeChanged(float newSize);
    }

    //------------------------------------------------------------------------------------

    /**
     * Listener for reaching end of the book
     */
    public interface OnTranscendCurrentBookListener {
        /**
         * Called when user is at the beginning of the book and is trying to navigate back
         */
        void onGoToPreviousBook();

        /**
         * Called when user reached the end of the book and is trying to navigate further
         */
        void onGoToNextBook();
    }


    //------------------------------------------------------------------------------------

    /**
     * Class used for storing results of calculation
     */
    private class CalculationResult {
        int totalPages = 0;
        int targetPage = 0;
        List<Position> positions = new ArrayList<>();
        SparseArray<SparseBooleanArray> hyphens = new SparseArray<>();
        SparseArray<SparseArray<Float>> spacings = new SparseArray<>();

        void addHyphen(int line, int index, boolean needMark) {
            if (hyphens.get(line) == null) {
                hyphens.put(line, new SparseBooleanArray());
            }
            hyphens.get(line).append(index, needMark);
        }

        void setSpacing(int line, int index, float value) {
            if (spacings.get(line) == null) {
                spacings.put(line, new SparseArray<Float>());
            }
            spacings.get(line).put(index, value);
        }

    }

    //-----------------------------------------------------------------------------------

    /**
     * Class used for storing contents buttons coordinates
     */
    private class ContentButton {
        private int page = 0;
        private int top = 0;
        private int bottom = 0;
        private int left = 0;
        private int right = 0;

        ContentButton(int page, int left, int right, int top, int bottom) {
            this.page = page;
            this.left = left;
            this.right = right;
            this.top = top;
            this.bottom = bottom;
        }

        boolean check(int page, int x, int y) {
            return (page == this.page && x >= this.left && x <= this.right && y >= this.top && y <= this.bottom);
        }

        RectF getRectangle() {
            return new RectF(this.left, this.top, this.right, this.bottom);
        }
    }

    //------------------------------------------------------------------------------------

    /**
     * Class used for storing link buttons coordinates
     */
    private class WordButton {
        private String word;
        private int line;
        private int index;
        private float left;
        private float right;
        private float top;
        private float bottom;

        WordButton(String word, int line, int index, float left, float right, float top, float bottom) {
            this.word = word;
            this.line = line;
            this.index = index;
            this.left = left;
            this.right = right;
            this.top = top;
            this.bottom = bottom;
        }

        boolean check(float x, float y) {
            return (x >= this.left && x <= this.right && y >= this.top && y <= this.bottom);
        }

        RectF getRectangle() {
            return new RectF(this.left, this.top, this.right, this.bottom);
        }

        String getWord() {
            return word;
        }

        int getLine() {
            return line;
        }

        int getIndex() {
            return index;
        }

    }

    //------------------------------------------------------------------------------------

    /**
     * Task that is used to calculate page positions and various additional info
     */
    private static class BookCalculatorTask extends AsyncTask<Void, Void, int[]> {

        WeakReference<BookReader> reader;

        BookCalculatorTask(BookReader reader) {
            this.reader = new WeakReference<>(reader);
        }

        private boolean isFinished = false;

        boolean isFinished() {
            return isFinished;
        }

        @Override
        protected int[] doInBackground(Void... arg0) {
            try {
                Book book0 = reader.get().book;
                List<String>[] bookText0 = reader.get().bookText;
                int[] pages = new int[book0.getChapters().size() + 1];
                pages[0] = reader.get().currentColumnCount;
                for (int i = 0; !isCancelled() && i < book0.getChapters().size(); i++) {
                    CalculationResult result = reader.get().calculate(book0, bookText0, i, null, onCancelListener);
                    pages[i + 1] = pages[i] + result.totalPages;
                    try {
                        Thread.sleep(100);
                    } catch (Exception ex) {
                        //Do nothing
                    }
                }
                return pages;
            } catch (NullPointerException ex) {
                return new int[0];
            }
        }

        @Override
        protected void onPostExecute(int[] pages) {
            isFinished = true;
            BookReader r = reader.get();
            if (r != null) {
                r.globalSize = pages[pages.length - 1];
                r.globalPage = (r.chapter < 0 || r.chapter >= pages.length) ? 0 : pages[r.chapter] + r.page;
                r.globalIndicies = pages;
                r.invalidate();
            }
        }

        @Override
        protected void onCancelled() {
            isFinished = true;
        }

        @Override
        protected void onPreExecute() {
            isFinished = false;
            BookReader r = reader.get();
            if (r != null) {
                r.globalSize = 0;
                r.globalPage = 0;
            }
        }

        private BookCalculatorCancelListener onCancelListener = new BookCalculatorCancelListener() {
            @Override
            public boolean isCancelled() {
                return BookCalculatorTask.this.isCancelled();
            }
        };

    }

    /**
     * Interface for checking whether calculation task should be canceled
     */
    private interface BookCalculatorCancelListener {
        boolean isCancelled();
    }

    private BookCalculatorCancelListener alwaysFalseCancelListener = new BookCalculatorCancelListener() {
        @Override
        public boolean isCancelled() {
            return false;
        }
    };


}