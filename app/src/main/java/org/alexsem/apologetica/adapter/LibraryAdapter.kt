package org.alexsem.apologetica.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.alexsem.apologetica.R
import org.alexsem.apologetica.model.Book

class LibraryAdapter(private val context: Context,
                     private val itemClickListener: (Book) -> Unit)
    : RecyclerView.Adapter<LibraryAdapter.Holder>() {

    private var items = mutableListOf<Book>()

    fun updateData(items: List<Book>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    //----------------------------------------------------------------------------------------------

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, index: Int) =
        Holder(LayoutInflater.from(context).inflate(R.layout.library_book, parent, false))

    override fun onBindViewHolder(holder: LibraryAdapter.Holder, index: Int) {
        val book = items[index]
        holder.title.text = book.title
        holder.author.text = book.author
    }

    //----------------------------------------------------------------------------------------------

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.book_title)
        val author: TextView = itemView.findViewById(R.id.book_author)

        init {
            itemView.setOnClickListener { itemClickListener(items[adapterPosition]) }
        }
    }

}