package org.alexsem.apologetica.model

class Book(val id: Int) {
    var title = ""
    var author = ""
    var chapters = generateChapters(1)

    fun generateChapters(count: Int) =
        (1..count).map { i ->
            Chapter(id * 100 + i).apply {
                title = ""
                bookId = this@Book.id
            }
        }
}