package org.alexsem.apologetica.model

import android.graphics.Color

class OptionsMain {

    @JvmField var orientation = 0
    @JvmField var animation = true

    @JvmField var colPort = 1
    @JvmField var colLand = 2

    @JvmField var hyphenateMode = 2
    @JvmField var fillLine = false


    @JvmField var fc_back_color_day = Color.parseColor("#cfbf9e")
    @JvmField var fc_back_color_night = -0x1000000
    @JvmField var fc_text_color_day = -0x1000000
    @JvmField var fc_text_color_night = -0x222223
    @JvmField var fc_text_color_cinnabar = Color.parseColor("#6d4c41")
    @JvmField var fc_other_color_service = Color.parseColor("#6d4c41")

    @JvmField val fc_text_color_selected = -0x5fe0

    @JvmField val fc_text_color_dict = -0x3f7f01
    @JvmField val fc_text_color_error = -0x7f80

    @JvmField var fc_text_face = "Roboto-Regular"
    @JvmField var fc_text_bold = "Roboto-Bold"
    @JvmField var fc_text_italic = "Roboto-Italic"
    @JvmField var fc_text_title = "RobotoSlab-Bold"
    @JvmField var fc_text_size = 18f
    @JvmField var fc_text_spacing = 1.25f

    @JvmField var daytime = true

    fun fc_back_color() = if (daytime) fc_back_color_day else fc_back_color_night
    fun fc_text_color() = if (daytime) fc_text_color_day else fc_text_color_night
}
