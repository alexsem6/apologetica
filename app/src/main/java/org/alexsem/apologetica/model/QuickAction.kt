package org.alexsem.apologetica.model


/**
 * Describes all available quick actions
 */
enum class QuickAction (val code: Int) {

    NONE(0),
    GOTO_NEXT_PAGE_LONG(1),
    GOTO_PREVIOUS_PAGE_LONG(2),
    GOTO_NEXT_PAGE_1(3),
    GOTO_PREVIOUS_PAGE_1(4),
    GOTO_NEXT_PAGE_5(5),
    GOTO_PREVIOUS_PAGE_5(6),
    GOTO_NEXT_PAGE_10(7),
    GOTO_PREVIOUS_PAGE_10(8),
    GOTO_FIRST_PAGE(9),
    GOTO_LAST_PAGE(10),
    GOTO_NEXT_CHAPTER(11),
    GOTO_PREVIOUS_CHAPTER(12),
    GOTO_NEXT_BOOK(31),
    GOTO_PREVIOUS_BOOK(32),
    NAVIGATE_MENU_MAIN(13),
    NAVIGATE_MENU_OTHER(14),
    NAVIGATE_CONTENTS(15),
    NAVIGATE_BOOKMARKS(16),
    ADD_BOOKMARK_QUICK(17),
    NAVIGATE_HISTORY(18),
    GOTO_HISTORY_RECENT(19),
    NAVIGATE_LIBRARY(20),
    NAVIGATE_SETTINGS_MAIN(21),
    NAVIGATE_SETTINGS_FONTS(22),
    NAVIGATE_SETTINGS_ACTIONS(23),
    MODE_SWITCH_DAYNIGHT(24),
    MODE_SWITCH_DICTIONARY(25),
    MODE_SWITCH_ERRORS(26),
    BRIGHTNESS_INCREASE(27),
    BRIGHTNESS_DECREASE(28),
    NAVIGATE_MANUAL(29),
    NAVIGATE_EXIT(30);


    companion object {

        /**
         * Return QuickAction which corresponds to the specific code
         * @param code Code to search for
         * @return Respective QuickAction or null if no code found
         */
        fun getByCode(code: Int): QuickAction {
            var firstShot: QuickAction? = null
            if (code > -1 && code < values().size) {
                firstShot = values()[code]
            }
            if (firstShot != null && firstShot.code == code) {
                return firstShot
            }
            for (action in values()) {
                if (action.code == code) {
                    return action
                }
            }
            return NONE
        }
    }
}
