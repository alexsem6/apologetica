package org.alexsem.apologetica.model;

import java.util.HashMap;
import java.util.Map;

public class OptionsActions {

    public Map<ReaderZone, QuickAction> actionsShort = new HashMap<>();
    public Map<ReaderZone, QuickAction> actionsLong = new HashMap<>();
    public Map<Boolean, QuickAction> actionsVolume = new HashMap<>();
}
