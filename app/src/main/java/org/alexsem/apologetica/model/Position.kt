package org.alexsem.apologetica.model

/**
 * Class for storing position of text on page
 */
class Position {
    val chapterNumber: Int
    val lineNumber: Int
    val charIndex: Int

    @JvmOverloads constructor(chapterNumber: Int, lineNumber: Int = -1, charIndex: Int = 0) {
        this.chapterNumber = chapterNumber
        this.lineNumber = lineNumber
        this.charIndex = charIndex
    }

    constructor(copy: Position) {
        this.chapterNumber = copy.chapterNumber
        this.lineNumber = copy.lineNumber
        this.charIndex = copy.charIndex
    }

    constructor(string: String) {
        val data = string.split(";").map { it.toInt() }
        this.chapterNumber = data[0]
        this.lineNumber = data[1]
        this.charIndex = data[2]
    }

    override fun toString() = "$chapterNumber;$lineNumber;$charIndex"

    /**
     * Checks whether current position matches specified numbers
     * @param chapterNumber Chapter number to check
     * @param lineNumber Line number to check
     * @param charIndex Character index to check
     * @return true if check is successful, false otherwise
     */
    fun checkPosition(chapterNumber: Int, lineNumber: Int, charIndex: Int) =
        (this.chapterNumber == chapterNumber && this.lineNumber == lineNumber && this.charIndex == charIndex)

}
