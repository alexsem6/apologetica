@file:Suppress("DEPRECATION")

package org.alexsem.apologetica.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.AsyncTask
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import org.alexsem.apologetica.R
import org.alexsem.apologetica.databinding.ActivityReaderBinding
import org.alexsem.apologetica.model.Book
import org.alexsem.apologetica.model.OptionsActions
import org.alexsem.apologetica.model.OptionsMain
import org.alexsem.apologetica.model.Position
import org.alexsem.apologetica.model.QuickAction
import org.alexsem.apologetica.model.ReaderZone
import org.alexsem.apologetica.util.OptionsManager
import org.alexsem.apologetica.util.getNextBook
import org.alexsem.apologetica.util.getPreviousBook
import org.alexsem.apologetica.util.loadBookMetadata
import org.alexsem.apologetica.util.loadBookText
import org.alexsem.apologetica.view.BookReader
import java.io.IOException
import java.lang.ref.WeakReference

private const val REQUEST_LIBRARY = 287

class ReaderActivity : AppCompatActivity() {

    private lateinit var optionsMain: OptionsMain
    private lateinit var optionsActions: OptionsActions
    private var currentBookId = -1

    private lateinit var binding: ActivityReaderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReaderBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //-------------------- Options --------------------
        optionsMain = OptionsManager.loadSettings(this)
        optionsActions = OptionsManager.loadActions(this)

        //-------------------- Reader field --------------------
        binding.reader.setOptions(optionsMain, Typeface.DEFAULT)
        binding.reader.setTextSizeListener(readerTextSizeListener)
        binding.reader.setQuickActionListener(readerQuickActionsListener)
        binding.reader.setBrightnessListener(readerBrightnessChangedListener)
        binding.reader.setTranscendCurrentBookListener(readerTranscendCurrentBookListener)

        //-------------------- Toolbar --------------------
        setSupportActionBar(binding.readerToolbar)
        binding.readerInterceptor.setOnClickListener { switchFullScreen(true) }
        switchFullScreen(true)

        //-------------------- Load book --------------------
        if (OptionsManager.isInitialized(this)) {
            showSpecificBook(OptionsManager.loadCurrentBookId(this))
        } else {
            startActivityForResult(Intent(this, LibraryActivity::class.java), REQUEST_LIBRARY)
        }
    }

    override fun onPause() {
        super.onPause()
        if (currentBookId > -1) {
            switchFullScreen(true)
            OptionsManager.setInitialized(this)
            OptionsManager.saveCurrentBookId(this, currentBookId)
            binding.reader.currentPosition?.let { p ->
                OptionsManager.saveBookPosition(
                    this,
                    currentBookId,
                    p
                )
            }
        }
    }

    //-------------------------------------------------------------------------------------------------

    /**
     * Load book with specified identifier
     * @param id Book identifier
     */
    private fun showSpecificBook(id: Int) {
        if (id > 0) {
            binding.reader.currentPosition?.let { p ->
                OptionsManager.saveBookPosition(
                    this,
                    currentBookId,
                    p
                )
            }
            currentBookId = id
            BookLoaderTask(this).execute(id)
        }
    }

    /**
     * Toggle full screen display
     */
    private fun switchFullScreen(fullscreen: Boolean) {
        if (fullscreen) {
            binding.readerAppbar.visibility = View.INVISIBLE
            binding.readerAppbar.layoutTransition.setDuration(0)
            supportActionBar?.hide()
            window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
            binding.readerInterceptor.visibility = View.GONE
        } else {
            binding.readerAppbar.visibility = View.VISIBLE
            binding.readerAppbar.layoutTransition.setDuration(250)
            supportActionBar?.show()
            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            window.addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
            binding.readerInterceptor.visibility = View.VISIBLE
        }
    }

    //-------------------------------------------------------------------------------------------------
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.reader, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_library -> {
            startActivityForResult(Intent(this, LibraryActivity::class.java), REQUEST_LIBRARY)
            true
        }

        else -> super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_LIBRARY) when (resultCode) {
            Activity.RESULT_OK -> showSpecificBook(data?.getIntExtra("bookId", -1) ?: -1)
            else -> if (!OptionsManager.isInitialized(this@ReaderActivity)) {
                this@ReaderActivity.finish()
            }
        }
    }

    //-------------------------------------------------------------------------------------------------

    /**
     * Listener for quick actions
     */
    private val readerQuickActionsListener = object : BookReader.OnQuickActionsListener {
        override fun onQuickActionTriggered(zone: ReaderZone, isLong: Boolean) {
            if (isLong) {
                performAction(optionsActions.actionsLong[zone] ?: QuickAction.NONE)
            } else {
                performAction(optionsActions.actionsShort[zone] ?: QuickAction.NONE)
            }
        }

        override fun checkNextPageLongAction(zone: ReaderZone): Boolean {
            return optionsActions.actionsShort[zone] === QuickAction.GOTO_NEXT_PAGE_LONG
        }

        override fun checkPreviousPageLongAction(zone: ReaderZone): Boolean {
            return optionsActions.actionsShort[zone] === QuickAction.GOTO_PREVIOUS_PAGE_LONG
        }
    }

    /**
     * Listener for changes in text size
     */
    private val readerTextSizeListener =
        BookReader.OnTextSizeChangedListener {
            OptionsManager.saveTextSize(
                this@ReaderActivity,
                it
            )
        }

    /**
     * Listener for brightness changes
     */
    private val readerBrightnessChangedListener =
        BookReader.OnBrightnessListener { window.attributes.screenBrightness = it }

    /**
     * Listener for reaching end of book
     */
    private val readerTranscendCurrentBookListener =
        object : BookReader.OnTranscendCurrentBookListener {
            override fun onGoToPreviousBook() {
                getPreviousBook(this@ReaderActivity, currentBookId)?.let { book ->
                    AlertDialog.Builder(this@ReaderActivity)
                        .setTitle(R.string.reader_previous_book_title)
                        .setMessage(
                            String.format(
                                getString(R.string.reader_previous_book_prompt),
                                book.title
                            )
                        )
                        .setPositiveButton(R.string.action_yes) { _, _ -> showSpecificBook(book.id) }
                        .setNegativeButton(R.string.action_no, null).show()
                }
            }

            override fun onGoToNextBook() {
                getNextBook(this@ReaderActivity, currentBookId)?.let { book ->
                    AlertDialog.Builder(this@ReaderActivity)
                        .setTitle(R.string.reader_next_book_title)
                        .setMessage(
                            String.format(
                                getString(R.string.reader_next_book_prompt),
                                book.title
                            )
                        )
                        .setPositiveButton(R.string.action_yes) { _, _ -> showSpecificBook(book.id) }
                        .setNegativeButton(R.string.action_no, null).show()
                }
            }
        }

    //-------------------------------------------------------------------------------------------------

    /**
     * Performs action (passed from  BookReader)
     * @param action Action to perform
     */
    fun performAction(action: QuickAction) = when (action) {
        QuickAction.GOTO_NEXT_PAGE_1 -> binding.reader.goToNextPage(1)
        QuickAction.GOTO_NEXT_PAGE_5 -> binding.reader.goToNextPage(5)
        QuickAction.GOTO_NEXT_PAGE_10 -> binding.reader.goToNextPage(10)
        QuickAction.GOTO_PREVIOUS_PAGE_1 -> binding.reader.goToPreviousPage(1)
        QuickAction.GOTO_PREVIOUS_PAGE_5 -> binding.reader.goToPreviousPage(5)
        QuickAction.GOTO_PREVIOUS_PAGE_10 -> binding.reader.goToPreviousPage(10)
        QuickAction.GOTO_FIRST_PAGE -> binding.reader.goToFirstPage()
        QuickAction.GOTO_LAST_PAGE -> binding.reader.goToLastPage()
        QuickAction.GOTO_NEXT_CHAPTER -> binding.reader.goToNextChapter()
        QuickAction.GOTO_PREVIOUS_CHAPTER -> binding.reader.goToPreviousChapter()
        QuickAction.GOTO_NEXT_BOOK -> readerTranscendCurrentBookListener.onGoToNextBook()
        QuickAction.GOTO_PREVIOUS_BOOK -> readerTranscendCurrentBookListener.onGoToPreviousBook()
        QuickAction.NAVIGATE_CONTENTS -> binding.reader.switchContents(true)
        QuickAction.MODE_SWITCH_DICTIONARY -> binding.reader.switchDictionary(true)
        QuickAction.MODE_SWITCH_ERRORS -> binding.reader.switchErrors(true)
        QuickAction.NAVIGATE_MENU_MAIN -> switchFullScreen(false)
//            QuickAction.NAVIGATE_MENU_OTHER -> {
//                this.openOptionsMenu()
//                if (this.menu != null) {
//                    this.menu.performIdentifierAction(R.id.menu_other, 0)
//                }
//            }
//            QuickAction.NAVIGATE_HISTORY -> {
//                if (bookmarks.isOpened()) {
//                    bookmarks.hide()
//                }
//                if (!history.isOpened()) {
//                    history.show()
//                }
//            }
//            QuickAction.GOTO_HISTORY_RECENT -> if (historyList != null && historyList.getCount() > 1) {
//                showSpecificBook((historyList.getAdapter() as HistoryAdapter).getItem(1).getBookId(), null)
//            }
//            QuickAction.NAVIGATE_BOOKMARKS -> {
//                if (history.isOpened()) {
//                    history.hide()
//                }
//                if (!bookmarks.isOpened()) {
//                    bookmarksAdd.setVisibility(if (displayedBook != null && !readerReader.isContentsShowing()) View.VISIBLE else View.GONE)
//                    bookmarks.show()
//                }
//            }
//            QuickAction.ADD_BOOKMARK_QUICK -> if (bookmarksAdd.isEnabled()) {
//                val name = generateBookmarkName()
//                addBookmark(name, 1)
//                Toast.makeText(this, String.format(getString(R.string.bookmarks_created), name), Toast.LENGTH_SHORT)
//                    .show()
//            }
//            QuickAction.NAVIGATE_LIBRARY -> {
//                val intent = Intent(this, LibraryActivity::class.java)
//                intent.putExtra("read", true)
//                if (displayedBook != null) {
//                    intent.putExtra("bookID", displayedBook.getId())
//                }
//                startActivityForResult(intent, REQUEST_MANAGER)
//            }
//            QuickAction.NAVIGATE_SETTINGS_MAIN -> {
//                if (bookmarks.isOpened()) {
//                    bookmarks.hide()
//                }
//                if (history.isOpened()) {
//                    history.hide()
//                }
//                startActivityForResult(Intent(this, EditPreferences::class.java), REQUEST_PREFERENCES)
//            }
//            QuickAction.NAVIGATE_SETTINGS_ACTIONS -> {
//                if (bookmarks.isOpened()) {
//                    bookmarks.hide()
//                }
//                if (history.isOpened()) {
//                    history.hide()
//                }
//                startActivityForResult(Intent(this, ActionsActivity::class.java), REQUEST_ACTIONS)
//            }
//            QuickAction.NAVIGATE_SETTINGS_FONTS -> {
//                if (bookmarks.isOpened()) {
//                    bookmarks.hide()
//                }
//                if (history.isOpened()) {
//                    history.hide()
//                }
//                val i1 = Intent(this, EditPreferences::class.java)
//                i1.putExtra("fonts", true)
//                startActivityForResult(i1, REQUEST_PREFERENCES)
//            }
        QuickAction.MODE_SWITCH_DAYNIGHT -> {
            optionsMain.daytime = !optionsMain.daytime
            binding.reader.setBackgroundColor(if (optionsMain.daytime) optionsMain.fc_back_color_day else optionsMain.fc_back_color_night)
            binding.reader.invalidate()
        }

        QuickAction.BRIGHTNESS_INCREASE -> binding.reader.increaseBrightness()
        QuickAction.BRIGHTNESS_DECREASE -> binding.reader.decreaseBrightness()
//            QuickAction.NAVIGATE_MANUAL -> startActivity(Intent(this, HelpActivity::class.java))
        QuickAction.NAVIGATE_EXIT -> this.finish()
        else -> {
        }
    }

    //-------------------------------------------------------------------------------------------------

    /**
     * Task for loading books
     */
    private class BookLoaderTask(activity: ReaderActivity) :
        AsyncTask<Int, Void, BookLoaderTask.Result?>() {

        private inner class Result(
            val book: Book,
            val text: Array<List<String>>,
            val position: Position
        )

        private val activity = WeakReference(activity)

        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {
            super.onPreExecute()
            activity.get()?.let { a ->
                a.binding.progress.visibility = View.VISIBLE
            }
        }

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg params: Int?): Result? {
            val bookId = params[0] ?: -1
            return try {
                activity.get()?.let { a ->
                    val book = loadBookMetadata(a, bookId)
                    val text = loadBookText(a, bookId)
                    val position = OptionsManager.loadBookPosition(a, bookId)
                    Result(book, text, position)
                }
            } catch (ex: IOException) {
                null
            }
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(result: Result?) {
            super.onPostExecute(result)
            activity.get()?.let { a ->
                a.binding.progress.visibility = View.GONE
                if (result != null) {
                    a.title = result.book.title
                    a.binding.reader.setData(result.book, result.text, result.position)
                    OptionsManager.saveCurrentBookId(a, result.book.id)
                    OptionsManager.saveBookPosition(a, result.book.id, result.position)
                } else {
                    Snackbar.make(
                        a.binding.reader,
                        R.string.reader_load_failed,
                        Snackbar.LENGTH_INDEFINITE
                    )
                        .apply {
                            setAction(R.string.action_retry) { BookLoaderTask(a).execute(a.currentBookId) }
                        }.show()
                }
            }
        }
    }

}
