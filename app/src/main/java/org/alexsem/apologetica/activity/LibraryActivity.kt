@file:Suppress("DEPRECATION")

package org.alexsem.apologetica.activity

import android.app.Activity
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import org.alexsem.apologetica.adapter.LibraryAdapter
import org.alexsem.apologetica.databinding.ActivityLibraryBinding
import org.alexsem.apologetica.model.Book
import org.alexsem.apologetica.util.OptionsManager
import org.alexsem.apologetica.util.loadBooks
import java.lang.ref.WeakReference

class LibraryActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLibraryBinding
    private var lastPosition: Parcelable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLibraryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //-------------------- Action bar --------------------
        setSupportActionBar(binding.libraryToolbar)
        if (OptionsManager.isInitialized(this)) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        //-------------------- Setup list --------------------
        binding.libraryList.layoutManager =
            LinearLayoutManager(this)
        binding.libraryList.itemAnimator =
            DefaultItemAnimator()
        binding.libraryList.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )
        val adapter = LibraryAdapter(this) { book ->
            setResult(Activity.RESULT_OK, Intent().apply {
                putExtra("bookId", book.id)
            })
            this@LibraryActivity.finish()
        }
        binding.libraryList.adapter = adapter

        //-------------------- Load list of books --------------------
        LoadLibraryTask(this).execute()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable("list", binding.libraryList.layoutManager?.onSaveInstanceState())
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        lastPosition = savedInstanceState.getParcelable("list")
        binding.libraryList.layoutManager?.onRestoreInstanceState(lastPosition)
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Task used for loading issues
     */
    private class LoadLibraryTask(activity: LibraryActivity) : AsyncTask<Void, Void, List<Book>>() {

        private val activity = WeakReference(activity)

        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {
            super.onPreExecute()
            activity.get()?.binding?.libraryProgress?.visibility = View.VISIBLE
        }

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg params: Void): List<Book> {
            return try {
                loadBooks(activity.get()!!)
            } catch (ex: Exception) {
                emptyList()
            }
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(result: List<Book>) {
            super.onPostExecute(result)
            activity.get()?.let { a ->
                a.binding.libraryProgress.visibility = View.GONE
                (a.binding.libraryList.adapter as LibraryAdapter).updateData(result)
                a.binding.libraryList.scheduleLayoutAnimation()
                a.binding.libraryList.layoutManager?.onRestoreInstanceState(a.lastPosition)
                a.lastPosition = null
                if (result.isEmpty()) {
                    if (a.binding.libraryEmpty.visibility != View.VISIBLE) {
                        a.binding.libraryEmpty.apply {
                            this.alpha = 0f
                            this.visibility = View.VISIBLE
                            this.animate()
                                .alpha(1f)
                                .setDuration(500)
                                .setListener(null)
                        }
                    }
                } else {
                    a.binding.libraryEmpty.visibility = View.GONE
                }
            }

        }
    }

}
