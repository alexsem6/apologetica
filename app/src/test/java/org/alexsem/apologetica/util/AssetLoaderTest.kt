package org.alexsem.apologetica.util

import android.content.Context
import android.graphics.Typeface
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import java.io.IOException

@RunWith(RobolectricTestRunner::class)
class AssetLoaderTest {

    private lateinit var instrumentationCtx: Context

    @Before
    fun setup() {
        instrumentationCtx = RuntimeEnvironment.application
    }

    @Test
    fun testGetBookCount() {
        assertEquals("Incorrect number of books", 13, getBookCount(instrumentationCtx))
    }

    @Test
    fun testGetNextBookId() {
        assertEquals("Incorrect id of the next book", 12, getNextBook(instrumentationCtx, 11)?.id)
        assertNull("Next book should not be available", getNextBook(instrumentationCtx, 13))
    }

    @Test
    fun testGetPreviousBookId() {
        assertEquals("Incorrect id of the previous book", 10, getPreviousBook(instrumentationCtx, 11)?.id)
        assertNull("Previous book should not be available", getPreviousBook(instrumentationCtx, 1))
    }

    @Test
    fun testLoadBookMetadata() {
        val book = loadBookMetadata(instrumentationCtx, 1)
        assertEquals("Incorrect book title", "La doctrina ortodoxa sobre la salvación", book.title)
        assertEquals("Incorrect book author", "J. Maximov", book.author)
    }

    @Test
    fun testLoadBookText() {
        val text: Array<List<String>> = loadBookText(instrumentationCtx, 1)
        assertTrue("Text loaded incorrectly", text.isNotEmpty())
        assertTrue("Text loaded incorrectly", text[0].isNotEmpty())
        assertEquals("Text loaded incorrectly", '[', text[0][0][0])
    }

    @Test
    fun testLoadTypeface() {
        assertNotNull("Typeface loading failed", loadTypeface(instrumentationCtx, "RobotoSlab-Bold", Typeface.DEFAULT_BOLD))
        assertEquals("Default value is incorrect", Typeface.DEFAULT_BOLD, loadTypeface(instrumentationCtx, "unknown", Typeface.DEFAULT_BOLD))
    }

    @Test(expected = IOException::class)
    fun testLoadBookMetadataIncorrect() {
        loadBookMetadata(instrumentationCtx, 500)
    }

    @Test(expected = IOException::class)
    fun testLoadBookTextIncorrect() {
        loadBookText(instrumentationCtx, 500)
    }
}